## This is the code for sponsorship sharing app published in google play. I used a parse database to store the data , and used a server side java script to communicate between the client accessible database and the private database controled through specific security and ACL to store sensitive data.

## The app was stopped due to lack of time and effort (I was alone working WE and after work hours ) to maintain the users demands and difficult client behaviour which lead to unintend bugs.

Here we can see on google play how the app was designed

![VosLiensPresentation](/images/vosliens.jpg)

Here we can see the app in google play

![VosLiens_in_google_play](/images/vosliens2.png)

Here we can see the app release stats

![VosLiens_release](/images/release.png)

And finally some promising but also challenging reviews

![VosLiens_reviews](/images/avis.png)

