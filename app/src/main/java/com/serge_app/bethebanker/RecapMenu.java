package com.serge_app.bethebanker;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.parse.ParseUser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.serge_app.bethebanker.MyExpandableListAdapterTick.check_states;

//import android.support.v7.app.AppCompatActivity;

public class RecapMenu extends AppCompatActivity {

    ExpandableListView expandableListView;





    List<String> Banks;



    Map<String, List<String>> Recap;

    ExpandableListAdapter listAdapter;

    String coderecu = "Non défini par l'utilisateur";
    public boolean onCreateOptionsMenu(Menu menu) {

                /* ParseACL defaultACL = new ParseACL();
                defaultACL.setPublicReadAccess(true);
                defaultACL.setPublicWriteAccess(true);
                ParseACL.setDefaultACL(defaultACL, true);*/

        MenuInflater menuInflater = new MenuInflater(this);

        menuInflater.inflate(R.menu.bethebankermenu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if(item.getItemId()==R.id.MainPage)
        {

            Intent graphintent = new Intent(this,ActivityChoice.class);
            startActivity(graphintent);

        }

        if(item.getItemId()==R.id.GraphPage)
        {

            Intent graphintent = new Intent(this,GraphActivity.class);
            startActivity(graphintent);

        }

        if(item.getItemId()==R.id.Disclaimer)
        {
            Intent graphintent = new Intent(this,PolitiqueConfidentalite.class);
            startActivity(graphintent);

        }

        if(item.getItemId()==R.id.Classement)
        {
            Intent scrollList = new Intent(this,ScrollListActivity.class);
            startActivity(scrollList);


        }

        if(item.getItemId() == R.id.Logout) {

            ParseUser.logOut();


                SharedPreferences settings3 = getSharedPreferences("MYPREFS", 0);
                SharedPreferences.Editor editor3 = settings3.edit();
                try {
                    editor3.putString("CBSTATES", ObjectSerializer.serialize(check_states));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                editor3.commit();


            Intent intent = new Intent(getApplicationContext(),MainActivity.class);

            startActivity(intent);

        }

        if(item.getItemId() == R.id.Destroy) {

            Intent intent = new Intent(getApplicationContext(),DestroyUser.class);

            startActivity(intent);

        }

        if(item.getItemId() == R.id.UploadMail) {

            Intent intent = new Intent(getApplicationContext(),UpdateMail.class);

            startActivity(intent);

        }



        return super.onOptionsItemSelected(item);
    }


    public void comp_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), ScrollListActivity.class);
        startActivity(graphintent);

    }

    public void graph_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), GraphActivity.class);
        startActivity(graphintent);

    }

    public void settings_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), UpdateMail.class);
        startActivity(graphintent);

    }

    public void mail_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), RecapMenu.class);
        startActivity(graphintent);

    }

    public void backarrow(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), ActivityChoice.class);
        startActivity(graphintent);

    }




    public void fillDataRecap (Map<String, List<Map<String, String>> >  dico_recap){


        Banks = new ArrayList<>();

        Recap = new HashMap<>();



        //List<String> Banks = new ArrayList<>();
        Integer c = -1;
        for (String bank:  dico_recap.keySet() )
        {
            c = c+1;
            Banks.add(bank );

            List<Map<String,String>> list_map = dico_recap.get(bank);
            List<String> Contenu= new ArrayList<>();

            Log.i("maps", list_map.toString());
            for(Map<String,String> maps : list_map)
            {

                String parrainn = maps.keySet().toArray()[0].toString();
                Log.i("parrainn",parrainn);

                if(maps.get(parrainn) == null)
                {

                     coderecu = "Non défini par l'utilisateur";
                }
                else {
                     coderecu = maps.get(parrainn);
                }


                Contenu.add("Votre parrain : " + parrainn + " et votre code : " + coderecu) ;

                Log.i("Contenu", Contenu.toString());

                Recap.put(Banks.get(c), Contenu);
                Log.i("Recap", Recap.toString());
                Log.i("Banks", Banks.toString());

            }



        }




    }
/*
    protected void onStop() {
        SharedPreferences settings3 = getSharedPreferences("MYPREFS", 0);
        SharedPreferences.Editor editor3 = settings3.edit();
        try {
            editor3.putString("CBSTATES", ObjectSerializer.serialize(check_states));
        } catch (IOException e) {
            e.printStackTrace();
        }
        editor3.commit();
    }*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recap);



        Map<String, List<Map<String, String>> >  dico_recap =  ParseUser.getCurrentUser().getMap("ReceptionMenu");

        if (dico_recap == null)
        {
            Toast.makeText(getApplicationContext() ,"Aucune Réception Pour le moment" ,Toast.LENGTH_LONG).show();
        }

        if(dico_recap != null)
        {

            if (dico_recap.isEmpty())
            {
                Toast.makeText(getApplicationContext() ,"Aucune Réception Pour le moment" ,Toast.LENGTH_LONG).show();

            }

            else{

            //Log.i("Map of List" , dico_recap.toString());


            expandableListView = (ExpandableListView) findViewById(R.id.ExpRecap);
            fillDataRecap(dico_recap);


            listAdapter = new MyExpandableListAdapterTick(this, Banks, Recap);

                if(expandableListView != null)
                {
                    //Log.i("Ladapter",listAdapter.toString());

                    expandableListView.setAdapter(listAdapter);
                }

                SharedPreferences settings3 = getSharedPreferences("MYPREFS", 0);
                try {
                    check_states = (ArrayList<ArrayList<Integer>>) ObjectSerializer.deserialize(settings3.getString("CBSTATES", ObjectSerializer.serialize(new ArrayList<ArrayList<Integer>>())));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }









   /*             LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService( getApplicationContext().LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.listchild_tick,null);*/



     /*           for(Integer i=0 ;i<listAdapter.getGroupCount() ; i++) {
                    Integer childcount = listAdapter.getChildrenCount(i);
                    for(Integer j=0; j< childcount ; j++)
                    {
                        Log.i("childcount", String.valueOf(j));

                      View ChildView = listAdapter.getChildView(i,j ,true,view);


                    }
                }*/


        }

        }










    }
}
