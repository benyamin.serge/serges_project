package com.serge_app.bethebanker;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.parse.DeleteCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;

import java.util.HashMap;

//import android.support.v7.app.AlertDialog;

public class DestroyUser extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destroy_user);


        LayoutInflater DestroyUserinflater = (LayoutInflater) DestroyUser.this.getSystemService(DestroyUser.this.LAYOUT_INFLATER_SERVICE);


        AlertDialog.Builder DestroyUserbuilder = new AlertDialog.Builder(DestroyUser.this);


        DestroyUserbuilder.setView(R.layout.destroyuser_view);

        final View DestroyUser_view = DestroyUserinflater.inflate(R.layout.destroyuser_view, null);

        TextView tv = DestroyUser_view.findViewById(R.id.DestroyText);

        String str = "Voulez vous supprimer votre compte ?" +"<br>"+"Cette action est irréversible.";

        tv.setText(Html.fromHtml(str));



        //tv.setPadding(10,0,0,0);

        Log.i("tvtext" , tv.getText().toString());

        DestroyUserbuilder.setTitle("Suppression du compte");



        DestroyUserbuilder.setPositiveButton("Supprimer le compte", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                final ParseUser user = ParseUser.getCurrentUser();

                HashMap<String,String> params = new HashMap<String, String>();

                params.put("username",user.getUsername());


                ParseCloud.callFunctionInBackground("DestroyUser",params,  new FunctionCallback<String>() {
                    public void done(String str, ParseException e) {
                        if (e == null) {


                            user.deleteInBackground(new DeleteCallback() {
                                @Override
                                public void done(com.parse.ParseException e) {
                                    if (e == null) {
                                        Intent intent = new Intent(getApplicationContext(),MainActivity.class);

                                        startActivity(intent);
                                        ParseUser.getCurrentUser().logOut();
                                        Toast.makeText(getApplicationContext(), "Compte supprimé", Toast.LENGTH_LONG).show();
                                    } else {

                                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                    }

                                }

                            });


                        } else {
                            Log.i("messageparsedestroy",e.getMessage());
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }

                });


            }

        });

        DestroyUserbuilder.setNegativeButton("Revenir au menu principal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Intent intentt = new Intent(getApplicationContext(), ActivityChoice.class);

                startActivity(intentt);

            }
        });

        DestroyUserbuilder.show();
        DestroyUserbuilder.create();


    }

}
