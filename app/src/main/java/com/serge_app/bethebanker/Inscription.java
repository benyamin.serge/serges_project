package com.serge_app.bethebanker;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

//import android.support.v7.app.AppCompatActivity;

public class Inscription extends AppCompatActivity {



    public void SignUpMethod(View view) {

        EditText PW = (EditText) findViewById(R.id.PasswordIdInscription);
        final String password = PW.getText().toString();
        //Log.i("password",password);

        EditText UsernameEditText = (EditText) findViewById(R.id.UserIdInscription);
        final String Username = UsernameEditText.getText().toString();


        if (password == null | Username == null) {

        Toast.makeText(getApplicationContext(),"Choisissez un nom d'utilisateur et un mot de passe",Toast.LENGTH_LONG).show();;



        }

        if (password != null & Username != null)

        {

            ParseUser user_signup = new ParseUser();
            user_signup.setPassword(password);
            user_signup.setUsername(Username);


            user_signup.signUpInBackground(new SignUpCallback() {
                @Override
                public void done(ParseException e) {


                    if (e == null) {
                        Toast.makeText(Inscription.this, "Inscription effectuée avec succès", Toast.LENGTH_LONG).show();

                        //ParseUser.getCurrentUser().put()


                        redirectifSignedUp();

                    } else {
                        Log.i("exception", e.getMessage().toString());
                        Toast.makeText(Inscription.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                }
            });

        }


    }



    public void redirectifSignedUp() {

        if (ParseUser.getCurrentUser() != null) {
            Intent intent = new Intent(getApplicationContext(),Beginner_Activity.class);

            //intent.putExtra("flag",1);

            startActivity(intent);

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);
    }
}
