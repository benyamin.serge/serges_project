package com.serge_app.bethebanker;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
//import android.support.v7.app.ActionBar;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.ActionBar;
import android.util.ArrayMap;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import static com.parse.ParseUser.getCurrentUser;


public class SponsorActivity extends AppCompatActivity implements View.OnKeyListener, View.OnClickListener, DialogInterface.OnKeyListener {

    String id;
    Integer money;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    @Override
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i == keyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return false;
    }

    @Override
    public boolean onKey(DialogInterface dialog, int i, KeyEvent keyEvent) {
        if (i == keyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return false;
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.background_layout )

        {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        }


    }


    Integer valmax;
    Integer randomnumber;
    Integer ppd = 0;
    ExpandableListView expandableListView;

    String parrain_choisi;


    List<String> Institutions;


    Map<String, List<String>> Banks_and_Insurances;

    ExpandableListAdapter listAdapter;

    ArrayList<String> Friends_List = new ArrayList();

    final Map<String, String> dictionnaire = new HashMap<String, String>();

    final List<Map<String, String>> receiverdico = new ArrayList<Map<String, String>>();

    String name_to_catch = "";

    ParseUser user_got = ParseUser.getCurrentUser();

    String usernamefound = " ";




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        MenuInflater menuInflater = new MenuInflater(this);

        menuInflater.inflate(R.menu.bethebankermenu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.MainPage) {

            Intent graphintent = new Intent(this, ActivityChoice.class);
            startActivity(graphintent);

        }


        if (item.getItemId() == R.id.GraphPage) {

            Intent graphintent = new Intent(this, GraphActivity.class);
            startActivity(graphintent);

        }

        if (item.getItemId() == R.id.Disclaimer) {
            Intent graphintent = new Intent(this, PolitiqueConfidentalite.class);
            startActivity(graphintent);

        }

        if (item.getItemId() == R.id.Classement) {
            Intent scrollList = new Intent(this, ScrollListActivity.class);
            startActivity(scrollList);


        }

        if (item.getItemId() == R.id.Logout) {

            ParseUser.logOut();

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);

            startActivity(intent);

        }

        if (item.getItemId() == R.id.Destroy) {

            Intent intent = new Intent(getApplicationContext(), DestroyUser.class);

            startActivity(intent);

        }

        if (item.getItemId() == R.id.RecapReceive) {

            Intent intent = new Intent(getApplicationContext(), RecapMenu.class);

            startActivity(intent);

        }
        if (item.getItemId() == R.id.UploadMail) {

            Intent intent = new Intent(getApplicationContext(), UpdateMail.class);

            startActivity(intent);

        }




        return super.onOptionsItemSelected(item);
    }



    public void comp_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), ScrollListActivity.class);
        startActivity(graphintent);

    }

    public void graph_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), GraphActivity.class);
        startActivity(graphintent);

    }

    public void settings_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), UpdateMail.class);
        startActivity(graphintent);

    }

    public void mail_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), RecapMenu.class);
        startActivity(graphintent);

    }

    public void backarrow(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), ActivityChoice.class);
        startActivity(graphintent);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__screen);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Trouve ton Filleul");

        ImageView imageButton = (ImageView) findViewById(R.id.logoImage2);


        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent graphintent = new Intent(getApplicationContext(), ActivityChoice.class);
                startActivity(graphintent);


            }
        });




        RelativeLayout bckgound = (RelativeLayout) findViewById(R.id.background_layout);



        bckgound.setOnClickListener(SponsorActivity.this);



        expandableListView = (ExpandableListView) findViewById(R.id.ExpandableListView);
        fillData();

        listAdapter = new MyExpandableListAdapter(this, Institutions, Banks_and_Insurances);
        expandableListView.setAdapter(listAdapter);


        //TextView textView = (TextView) expandableListView.findViewById(R.id.ExpandableListView);

        // textView.setTextColor(/* some color */);

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {

                //Toast.makeText(MainScreen.this, Institutions.get(i) + " :" + Banks_and_Insurances.get(Institutions.get(i)).get(i1), Toast.LENGTH_LONG).show();

                final String String_caught = Banks_and_Insurances.get(Institutions.get(i)).get(i1);


                final ArrayList<String> liste_parrains = new ArrayList<String>();


                ParseQuery<ParseUser> querry = ParseUser.getQuery();

                querry.whereNotEqualTo("username", getCurrentUser().getUsername());

                querry.whereContains("Banks", String_caught);

                querry.findInBackground(new FindCallback<ParseUser>() {
                    @Override
                    public void done(List<ParseUser> objects, ParseException e) {

                        for (ParseUser ob : objects) {

                            liste_parrains.add(ob.getUsername().toString());


                        }

                    }
                });


                EditText textt = new EditText(SponsorActivity.this);


                AlertDialog.Builder builder = new AlertDialog.Builder(SponsorActivity.this);

                builder.setTitle("Informations requises");

                if (String_caught.equals("Boursorama") | String_caught.equals("Revolut") |String_caught.equals("Deliveroo")|
                        String_caught.equals("Uber")| String_caught.equals("Saveur Bière")|
                        String_caught.equals("Bolt")| String_caught.equals("MAX")   ) {

                    LayoutInflater Boursoinflater = (LayoutInflater) SponsorActivity.this.getSystemService(SponsorActivity.this.LAYOUT_INFLATER_SERVICE);

                    builder.setView(R.layout.boursorama_view);

                    final View Boursoview = Boursoinflater.inflate(R.layout.boursorama_view, null);


                    builder.setView(Boursoview, 86, 60, 10, 0);


                    builder.setPositiveButton("Envoyer les informations", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, int which) {

                            final EditText Link = (EditText) Boursoview.findViewById(R.id.Link);


                            if (!(Link.getText().toString().isEmpty())) {

                                if (String_caught.equals("Boursorama")) {

                                    HashMap<String, String> params = new HashMap<String, String>();

                                    params.put("username", ParseUser.getCurrentUser().getUsername());
                                    params.put("LienParrainage", Link.getText().toString());

                                    ParseCloud.callFunctionInBackground("LienParrainage", params, new FunctionCallback<String>() {
                                        public void done(String str, ParseException e) {
                                            if (e == null) {
                                                Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();
                                            } else {
                                                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                            }
                                        }

                                    });


                                }
                                if (String_caught.equals("Revolut")) {

                                    HashMap<String, String> params = new HashMap<String, String>();

                                    params.put("username", ParseUser.getCurrentUser().getUsername());
                                    params.put("FortuneoLink", Link.getText().toString());



                                    ParseCloud.callFunctionInBackground("FortuneoLink", params, new FunctionCallback<String>() {
                                        public void done(String str, ParseException e) {
                                            if (e == null) {
                                                Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();
                                            } else {
                                                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                            }
                                        }

                                    });


                                }

                                if (String_caught.equals("Saveur Bière")) {

                                    HashMap<String, String> params = new HashMap<String, String>();

                                    params.put("username", ParseUser.getCurrentUser().getUsername());
                                    params.put("SaveurBiere", Link.getText().toString());


                                    ParseCloud.callFunctionInBackground("SaveurBiere", params, new FunctionCallback<String>() {
                                        public void done(String str, ParseException e) {
                                            if (e == null) {
                                                Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();
                                            } else {
                                                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                            }
                                        }

                                    });


                                }



                                if (String_caught.equals("Uber")) {

                                    HashMap<String, String> params = new HashMap<String, String>();

                                    params.put("username", ParseUser.getCurrentUser().getUsername());
                                    params.put("CodeUber", Link.getText().toString());

                                    ParseQuery<ParseObject> query = ParseQuery.getQuery("Banker");

                                    ParseCloud.callFunctionInBackground("Uber", params, new FunctionCallback<String>() {
                                        public void done(String str, ParseException e) {
                                            if (e == null) {
                                                Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();
                                            } else {
                                                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                            }
                                        }

                                    });


                                }
                                if (String_caught.equals("Deliveroo")) {

                                    HashMap<String, String> params = new HashMap<String, String>();

                                    params.put("username", ParseUser.getCurrentUser().getUsername());
                                    params.put("Deliveroo", Link.getText().toString());

                                    ParseQuery<ParseObject> query = ParseQuery.getQuery("Banker");

                                    ParseCloud.callFunctionInBackground("Deliveroo", params, new FunctionCallback<String>() {
                                        public void done(String str, ParseException e) {
                                            if (e == null) {
                                                Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();
                                            } else {
                                                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                            }
                                        }

                                    });


                                }


                                if (String_caught.equals("Bolt")) {

                                    HashMap<String, String> params = new HashMap<String, String>();

                                    params.put("username", ParseUser.getCurrentUser().getUsername());
                                    params.put("Bolt", Link.getText().toString());

                                    ParseQuery<ParseObject> query = ParseQuery.getQuery("Banker");

                                    ParseCloud.callFunctionInBackground("Bolt", params, new FunctionCallback<String>() {
                                        public void done(String str, ParseException e) {
                                            if (e == null) {
                                                Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();
                                            } else {
                                                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                            }
                                        }

                                    });


                                }

                                if (String_caught.equals("MAX")) {

                                    HashMap<String, String> params = new HashMap<String, String>();

                                    params.put("username", ParseUser.getCurrentUser().getUsername());
                                    params.put("MAX", Link.getText().toString());

                                    ParseQuery<ParseObject> query = ParseQuery.getQuery("Banker");

                                    ParseCloud.callFunctionInBackground("MAX", params, new FunctionCallback<String>() {
                                        public void done(String str, ParseException e) {
                                            if (e == null) {
                                                Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();
                                            } else {
                                                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                            }
                                        }

                                    });


                                }

                                List<String> BList = ParseUser.getCurrentUser().getList("Banks");


                                if (BList == null) {


                                    ArrayList<String> DList = new ArrayList<String>();
                                    DList.add(String_caught);

                                    BList = DList;

                                    ParseUser.getCurrentUser().put("Banks", BList);

                                } else {
                                    if (BList.contains(String_caught)) {
                                        BList = BList;
                                    } else {
                                        Log.i("List doesn't contain:", String.valueOf(BList.contains(String_caught)));
                                        BList.add(String_caught);
                                    }
                                }

                                //Log.i("new bank list:", BList.toString());


                                ParseUser.getCurrentUser().put("Banks", BList);


                                ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if (e == null) {
                                            Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();

                                            Integer Counter = ParseUser.getCurrentUser().getInt("Counter");
                                            if (Counter != null) {

                                                Counter = Counter + 1;
                                            } else {

                                                Counter = 1;
                                            }

                                            ParseUser.getCurrentUser().put("Counter", Counter);
                                            ParseUser.getCurrentUser().saveInBackground();

                                        } else {
                                            Toast.makeText(SponsorActivity.this,
                                                    "Le téléchargement des données n'a pas fonctionné et le message est" + e.getMessage(),
                                                    Toast.LENGTH_LONG).show();

                                        }
                                    }
                                });
                            } else {

                                Toast.makeText(getApplicationContext(), "S'il vous plait remplissez votre Lien de Parraiange", Toast.LENGTH_LONG).show();
                            }


                        }
                    });

                    builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });
                    builder.show();
                    builder.create();
                }


                if (String_caught.equals("ING")) {



                    LayoutInflater INGinflater = (LayoutInflater) SponsorActivity.this.getSystemService(SponsorActivity.this.LAYOUT_INFLATER_SERVICE);
                    builder.setMessage("Renseignez vos details");
                    builder.setView(R.layout.ing_view);
                    //In case it gives you an error for setView(View) try
                    final View INGview = INGinflater.inflate(R.layout.ing_view, null);

                    builder.setView(INGview, 86, 30, 10, 0);


                    final EditText SurnameUser = (EditText) INGview.findViewById(R.id.SurnameUserING);
                    final EditText NameUser = (EditText) INGview.findViewById(R.id.NameUserING);
                    final EditText CodeUser = (EditText) INGview.findViewById(R.id.Code);


                    //InputMethodManager mgr = (InputMethodManager) getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                    //mgr.hideSoftInputFromWindow(PostalCodeUser.getWindowToken(), 0);

                    builder.setPositiveButton("Envoyer les informations", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            Log.i("Surname", String.valueOf(!(SurnameUser.getText().toString().isEmpty())));


                            if (!(SurnameUser.getText().toString().isEmpty())) {
                                Log.i("Surname", SurnameUser.getText().toString());

                                if (!(NameUser.getText().toString().isEmpty())) {

                                    if (!(CodeUser.getText().toString().isEmpty())) {


                                        //ParseUser.getCurrentUser().put("Surname", SurnameUser.getText().toString());
                                        //ParseUser.getCurrentUser().put("Name", NameUser.getText().toString());
                                        HashMap<String, String> params = new HashMap<String, String>();

                                        params.put("username", ParseUser.getCurrentUser().getUsername());
                                        params.put("Code", CodeUser.getText().toString());
                                        params.put("Name", NameUser.getText().toString());
                                        params.put("Surname", SurnameUser.getText().toString());

                                        ParseQuery<ParseObject> query = ParseQuery.getQuery("Banker");

                                        ParseCloud.callFunctionInBackground("ING", params, new FunctionCallback<String>() {
                                            public void done(String str, ParseException e) {
                                                if (e == null) {
                                                    Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();
                                                } else {
                                                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                                }
                                            }

                                        });

                                        List<String> BList = ParseUser.getCurrentUser().getList("Banks");

                                        //Log.i("Blist:", BList.toString());

                                        if (BList == null) {

                                            ArrayList<String> DList = new ArrayList<String>();
                                            DList.add(String_caught);

                                            ParseUser.getCurrentUser().put("Banks", DList);

                                        } else {
                                            if (BList.contains(String_caught)) {
                                                BList = BList;
                                                ParseUser.getCurrentUser().put("Banks", BList);
                                            } else {
                                                Log.i("List doesn't contain:", String.valueOf(BList.contains(String_caught)));
                                                BList.add(String_caught);
                                                ParseUser.getCurrentUser().put("Banks", BList);
                                            }
                                        }


                                        ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
                                            @Override
                                            public void done(ParseException e) {
                                                if (e == null) {
                                                    //Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();
                                                    Integer Counter = ParseUser.getCurrentUser().getInt("Counter");
                                                    if (Counter != null) {

                                                        Counter = Counter + 1;
                                                    } else {

                                                        Counter = 1;
                                                    }

                                                    ParseUser.getCurrentUser().put("Counter", Counter);
                                                    ParseUser.getCurrentUser().saveInBackground();
                                                }
                                            }
                                        });
                                    } else {

                                        Toast.makeText(getApplicationContext(), "S'il vous plait remplissez votre Code", Toast.LENGTH_LONG).show();
                                    }

                                } else {

                                    Toast.makeText(getApplicationContext(), "S'il vous plait remplissez votre Nom", Toast.LENGTH_LONG).show();
                                }

                            }
                            if (SurnameUser.getText().toString().isEmpty()) {

                                Toast.makeText(getApplicationContext(), "S'il vous plait remplissez votre Prénom", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                    builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });
                    builder.show();
                    builder.create();
                }

                if (String_caught.equals("Fitness Park La Défense")) {



                    LayoutInflater fitnessinflater = (LayoutInflater) SponsorActivity.this.getSystemService(SponsorActivity.this.LAYOUT_INFLATER_SERVICE);
                    builder.setMessage("Renseignez vos details");
                    builder.setView(R.layout.fitness_view);
                    //In case it gives you an error for setView(View) try
                    final View fitnessview = fitnessinflater.inflate(R.layout.fitness_view, null);

                    builder.setView(fitnessview, 86, 30, 10, 0);


                    final EditText SurnameUser = (EditText) fitnessview.findViewById(R.id.SurnameUser);
                    final EditText NameUser = (EditText) fitnessview.findViewById(R.id.NameUser);
                    //final EditText CodeUser = (EditText) fitnessview.findViewById(R.id.Code);


                    //InputMethodManager mgr = (InputMethodManager) getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                    //mgr.hideSoftInputFromWindow(PostalCodeUser.getWindowToken(), 0);

                    builder.setPositiveButton("Envoyer les informations", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {





                            if (!(SurnameUser.getText().toString().isEmpty())) {
                                Log.i("Surname", SurnameUser.getText().toString());

                                if (!(NameUser.getText().toString().isEmpty())) {



                                        //ParseUser.getCurrentUser().put("Surname", SurnameUser.getText().toString());
                                        //ParseUser.getCurrentUser().put("Name", NameUser.getText().toString());
                                        HashMap<String, String> params = new HashMap<String, String>();

                                        params.put("username", ParseUser.getCurrentUser().getUsername());
                                        params.put("FitnessParkName", NameUser.getText().toString());
                                        params.put("FitnessParkSurname", SurnameUser.getText().toString());

                                        ParseQuery<ParseObject> query = ParseQuery.getQuery("Banker");

                                        ParseCloud.callFunctionInBackground("FitnessPark", params, new FunctionCallback<String>() {
                                            public void done(String str, ParseException e) {
                                                if (e == null) {
                                                    Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();
                                                } else {
                                                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                                }
                                            }

                                        });

                                        List<String> BList = ParseUser.getCurrentUser().getList("Banks");

                                        //Log.i("Blist:", BList.toString());

                                        if (BList == null) {

                                            ArrayList<String> DList = new ArrayList<String>();
                                            DList.add(String_caught);

                                            ParseUser.getCurrentUser().put("Banks", DList);

                                        } else {
                                            if (BList.contains(String_caught)) {
                                                BList = BList;
                                                ParseUser.getCurrentUser().put("Banks", BList);
                                            } else {
                                                Log.i("List doesn't contain:", String.valueOf(BList.contains(String_caught)));
                                                BList.add(String_caught);
                                                ParseUser.getCurrentUser().put("Banks", BList);
                                            }
                                        }


                                        ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
                                            @Override
                                            public void done(ParseException e) {
                                                if (e == null) {
                                                    //Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();
                                                    Integer Counter = ParseUser.getCurrentUser().getInt("Counter");
                                                    if (Counter != null) {

                                                        Counter = Counter + 1;
                                                    } else {

                                                        Counter = 1;
                                                    }

                                                    ParseUser.getCurrentUser().put("Counter", Counter);
                                                    ParseUser.getCurrentUser().saveInBackground();
                                                }
                                            }
                                        });


                                } else {

                                    Toast.makeText(getApplicationContext(), "S'il vous plait remplissez votre Nom", Toast.LENGTH_LONG).show();
                                }

                            }
                            if (SurnameUser.getText().toString().isEmpty()) {

                                Toast.makeText(getApplicationContext(), "S'il vous plait remplissez votre Prénom", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                    builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });
                    builder.show();
                    builder.create();
                }

                if (String_caught.equals("GMF") |String_caught.equals("Direct Energie") |String_caught.equals("AXA")| String_caught.equals("Bourse Direct")| String_caught.equals("American Express")  ) {

                    if (String_caught.equals("GMF") |String_caught.equals("Direct Energie")  ) {
                        LayoutInflater AXAinflater = (LayoutInflater) SponsorActivity.this.getSystemService(SponsorActivity.this.LAYOUT_INFLATER_SERVICE);
                        builder.setMessage("Renseignez vos détails");
                        builder.setView(R.layout.axabanque_view);
                        //In case it gives you an error for setView(View) try
                        final View AXAview = AXAinflater.inflate(R.layout.axabanque_view, null);

                        builder.setView(AXAview, 86, 5, 10, 0);


                        //final EditText SurnameUser = (EditText) AXAview.findViewById(R.id.SurnameUserING);
                        //final EditText NameUser = (EditText) AXAview.findViewById(R.id.NameUserAXA);
                        final EditText CodeUser = (EditText) AXAview.findViewById(R.id.Editcode);

                        builder.setPositiveButton("Envoyer les informations", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {


                                if (!(CodeUser.getText().toString().isEmpty())) {

                                    HashMap<String, String> params = new HashMap<String, String>();

                                    if(String_caught.equals("GMF")) {

                                        params.put("username", ParseUser.getCurrentUser().getUsername());
                                        params.put("CodeGMF", CodeUser.getText().toString());


                                        ParseQuery<ParseObject> query = ParseQuery.getQuery("Banker");

                                        ParseCloud.callFunctionInBackground("GMF", params, new FunctionCallback<String>() {
                                            public void done(String str, ParseException e) {
                                                if (e == null) {
                                                    Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();
                                                } else {
                                                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                                }
                                            }

                                        });
                                    }
                                    if(String_caught.equals("Direct Energie"))
                                    {

                                        params.put("username", ParseUser.getCurrentUser().getUsername());
                                        params.put("DirectEnergie", CodeUser.getText().toString());


                                        ParseQuery<ParseObject> query = ParseQuery.getQuery("Banker");

                                        ParseCloud.callFunctionInBackground("DirectEnergie", params, new FunctionCallback<String>() {
                                            public void done(String str, ParseException e) {
                                                if (e == null) {
                                                    Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();
                                                } else {
                                                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                                }
                                            }

                                        });


                                    }


                                    List<String> BList = ParseUser.getCurrentUser().getList("Banks");

                                    Log.i("Blist:", BList.toString());

                                    if (BList == null) {

                                        ArrayList<String> DList = new ArrayList<String>();
                                        DList.add(String_caught);

                                        ParseUser.getCurrentUser().put("Banks", DList);

                                    } else {
                                        if (BList.contains(String_caught)) {
                                            BList = BList;
                                            ParseUser.getCurrentUser().put("Banks", BList);
                                        } else {
                                            Log.i("List doesn't contain:", String.valueOf(BList.contains(String_caught)));
                                            BList.add(String_caught);
                                            ParseUser.getCurrentUser().put("Banks", BList);
                                        }
                                    }


                                    ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            if (e == null) {
                                                Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();
                                                Integer Counter = ParseUser.getCurrentUser().getInt("Counter");
                                                if (Counter != null) {

                                                    Counter = Counter + 1;
                                                } else {

                                                    Counter = 1;
                                                }

                                                ParseUser.getCurrentUser().put("Counter", Counter);
                                                ParseUser.getCurrentUser().saveInBackground();
                                            } else {
                                                Toast.makeText(SponsorActivity.this,
                                                        "Le téléchargement des données n'a pas fonctionné et le message est" + e.getMessage(),
                                                        Toast.LENGTH_LONG).show();

                                            }
                                        }
                                    });


                                } else {

                                    Toast.makeText(getApplicationContext(), "S'il vous plait remplissez votre numéro de sociétaire", Toast.LENGTH_LONG).show();
                                }


                            }
                        }).setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialog, int i, KeyEvent keyEvent) {
                                if (keyEvent.isShiftPressed() | keyEvent.getAction() == KeyEvent.KEYCODE_ENTER) {
                                    //&& keyEvent.getAction() == KeyEvent.ACTION_DOWN) {

                                    InputMethodManager imm = (InputMethodManager) getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
                                }
                                return false;
                            }

                        });

                        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                        builder.show();
                        builder.create();
                    } else {


                        LayoutInflater AXAinflater = (LayoutInflater) SponsorActivity.this.getSystemService(SponsorActivity.this.LAYOUT_INFLATER_SERVICE);
                        builder.setMessage("Renseignez vos détails");
                        builder.setView(R.layout.axaassurance_view);
                        //In case it gives you an error for setView(View) try
                        final View AXAview = AXAinflater.inflate(R.layout.axaassurance_view, null);

                        builder.setView(AXAview, 86, 5, 10, 0);


                        //final EditText SurnameUser = (EditText) AXAview.findViewById(R.id.SurnameUserING);
                        //final EditText NameUser = (EditText) AXAview.findViewById(R.id.NameUserAXA);
                        final EditText Link = (EditText) AXAview.findViewById(R.id.Editcode);

                        builder.setPositiveButton("Envoyer les informations", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {


                                if (!(Link.getText().toString().isEmpty())) {

                                    if (String_caught.equals("AXA")) {

                                        HashMap<String, String> params = new HashMap<String, String>();

                                        params.put("username", ParseUser.getCurrentUser().getUsername());
                                        params.put("AXAMail", Link.getText().toString());


                                        ParseQuery<ParseObject> query = ParseQuery.getQuery("Banker");

                                        ParseCloud.callFunctionInBackground("AXAMail", params, new FunctionCallback<String>() {
                                            public void done(String str, ParseException e) {
                                                if (e == null) {
                                                    Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();
                                                } else {
                                                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                                }
                                            }

                                        });
                                    }

                                    if (String_caught.equals("American Express")) {

                                        HashMap<String, String> params = new HashMap<String, String>();

                                        params.put("username", ParseUser.getCurrentUser().getUsername());
                                        params.put("AmericanExpressMail", Link.getText().toString());


                                        ParseCloud.callFunctionInBackground("American Express", params, new FunctionCallback<String>() {
                                            public void done(String str, ParseException e) {
                                                if (e == null) {
                                                    Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();
                                                } else {
                                                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                                }
                                            }

                                        });


                                    }
                                    if (String_caught.equals("Bourse Direct")) {

                                        HashMap<String, String> params = new HashMap<String, String>();

                                        params.put("username", ParseUser.getCurrentUser().getUsername());
                                        params.put("BourseDirect", Link.getText().toString());


                                        ParseCloud.callFunctionInBackground("Bourse Direct", params, new FunctionCallback<String>() {
                                            public void done(String str, ParseException e) {
                                                if (e == null) {
                                                    Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();
                                                } else {
                                                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                                }
                                            }

                                        });


                                    }


                                    List<String> BList = ParseUser.getCurrentUser().getList("Banks");

                                    Log.i("Blist:", BList.toString());

                                    if (BList == null) {

                                        ArrayList<String> DList = new ArrayList<String>();
                                        DList.add(String_caught);

                                        ParseUser.getCurrentUser().put("Banks", DList);

                                    } else {
                                        if (BList.contains(String_caught)) {
                                            BList = BList;
                                            ParseUser.getCurrentUser().put("Banks", BList);
                                        } else {
                                            Log.i("List doesn't contain:", String.valueOf(BList.contains(String_caught)));
                                            BList.add(String_caught);
                                            ParseUser.getCurrentUser().put("Banks", BList);
                                        }
                                    }


                                    ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            if (e == null) {
                                                Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();
                                                Integer Counter = ParseUser.getCurrentUser().getInt("Counter");
                                                if (Counter != null) {

                                                    Counter = Counter + 1;
                                                } else {

                                                    Counter = 1;
                                                }

                                                ParseUser.getCurrentUser().put("Counter", Counter);
                                                ParseUser.getCurrentUser().saveInBackground();
                                            } else {
                                                Toast.makeText(SponsorActivity.this,
                                                        "Le téléchargement des données n'a pas fonctionné et le message est" + e.getMessage(),
                                                        Toast.LENGTH_LONG).show();

                                            }
                                        }
                                    });


                                } else {

                                    Toast.makeText(getApplicationContext(), "S'il vous plait remplissez votre mail", Toast.LENGTH_LONG).show();
                                }


                            }
                        }).setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialog, int i, KeyEvent keyEvent) {
                                if (keyEvent.isShiftPressed() | keyEvent.getAction() == KeyEvent.KEYCODE_ENTER) {
                                    //&& keyEvent.getAction() == KeyEvent.ACTION_DOWN) {

                                    InputMethodManager imm = (InputMethodManager) getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
                                }
                                return false;
                            }

                        });

                        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                        builder.show();
                        builder.create();
                    }

                }


/*
                        final EditText Ville = (EditText) AXAview.findViewById(R.id.EditVille);
                        final EditText Adresse = (EditText) AXAview.findViewById(R.id.EditAdresse);
                        final EditText Mail = (EditText) AXAview.findViewById(R.id.EditMail);*/



                if (String_caught.equals("BNP Paribas") | String_caught.equals("Crédit Agricole") | String_caught.equals("Société Générale")) {

                    ParseQuery<ParseUser> query = ParseUser.getQuery();


                    query.whereNotEqualTo("username", getCurrentUser().getUsername());


                    query.whereContains("Banks", String_caught);


                    final List<Map<String, String>> receiverdico = new ArrayList<Map<String, String>>();

                    query.findInBackground(new FindCallback<ParseUser>() {
                        @Override
                        public void done(List<ParseUser> objects, ParseException e) {


                            if (e == null) {


                                Log.i("size", String.valueOf(objects.size()));
                                if (objects.size() > 0) {
                                    Random rand = new Random();

                                    final Map<String, String> dictionnaire = new HashMap<String, String>();


                                    Calendar actualtime = Calendar.getInstance();


                                    Map<String, Map<String, List<String>>> ReceiverList = ParseUser.getCurrentUser().getMap("ReceiverInfo");

                                    if ((ReceiverList != null)) {
                                        if (ReceiverList.isEmpty() == false) {
                                            for (String s : ReceiverList.keySet()) {
                                                if (ReceiverList.get(s).containsKey(String_caught)) {

                                                    if (String_caught == "Société Générale" | String_caught == "Crédit Agricole") {

                                                        Integer pp = ReceiverList.get(s).get(String_caught).size();

                                                        if (pp > 3) {

                                                            Toast.makeText(getApplicationContext(), "Vous avez peut etre dépassé le quota de parrainage autorisé", Toast.LENGTH_LONG).show();
                                                        }

                                                    }

                                                    if (String_caught == "BNP Paribas") {
                                                        Integer pp = 0;
                                                        Calendar calendar = Calendar.getInstance();

                                                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);

                                                        ParsePosition Position = new ParsePosition(0);

                                                        for (String key : ReceiverList.get(s).get(String_caught)) {

                                                            Date date = simpleDateFormat.parse(key, Position);

                                                            long diff = (calendar.getTime().getTime() - date.getTime()) / (1000 * 3600 * 24);
                                                            Log.i("diff en jours", String.valueOf(diff));

                                                            if (diff < 32) {
                                                                pp = pp + 1;

                                                            }
                                                        }

                                                        if (pp > 3) {

                                                            Toast.makeText(getApplicationContext(), "Vous avez peut etre dépassé le quota de parrainage autorisé", Toast.LENGTH_LONG).show();
                                                        }


                                                    }


                                                }


                                            }
                                        }
                                    }


                                    for (int j = 0; j < objects.size(); j++) {

                                        Map<String, Map<String, List<String>>> maplister = objects.get(j).getMap("ReceiverInfo");

                                        if (maplister != null) {


                                            if (maplister.containsKey(objects.get(j).get("username").toString())) {

                                                Map<String, List<String>> datelsit = maplister.get(objects.get(j).get("username").toString());


                                                if (datelsit != null) {


                                                    Integer pp = 0;

                                                    if (datelsit.containsKey(String_caught)) {
                                                        randomnumber = null;

                                                    } else {

                                                        randomnumber = j;
                                                        break;
                                                    }


                                                } else {

                                                    randomnumber = j;
                                                    break;
                                                }


                                            }

                                            if (!maplister.containsKey(objects.get(j).get("username").toString())) {

                                                randomnumber = j;
                                                break;
                                            }


                                        }
                                        if (maplister == null) {

                                            randomnumber = j;
                                            break;
                                        }


                                    }

                                    Log.i("randn", String.valueOf(randomnumber));

                                    if (randomnumber == null) {


                                        Toast.makeText(SponsorActivity.this, "Tous les filleuls présents ont déja été parrainés", Toast.LENGTH_LONG).show();
                                    }


                                    if (randomnumber != null) {


                                        name_to_catch = objects.get(randomnumber).getUsername().toString();

                                        dictionnaire.put("Username", name_to_catch);

                                        // Cloud Code here

                                        HashMap<String, String> params = new HashMap<String, String>();
                                        params.put("user", name_to_catch);
                                        //request.put("user",ParseUser.getCurrentUser());

                                        HashMap<String, String> privateinfos = new HashMap<String, String>();
                                        ParseCloud.callFunctionInBackground("RequestPrivateInfos", params, new FunctionCallback<HashMap<String, String>>() {
                                            public void done(HashMap<String, String> privateinfos, ParseException e) {
                                                if (e == null) {
                                                    Log.i("PRIVATE INFOS", privateinfos.toString());

                                                } else {
                                                    Log.i("error mess", e.getMessage());
                                                }
                                            }
                                        });

                                        if (privateinfos.get("Name") == null) {
                                            dictionnaire.put("Name", "Non défini par l'utilisateur");
                                        } else {

                                            dictionnaire.put("Name", privateinfos.get("Name"));

                                        }

                                        if (privateinfos.get("Surname") == null) {
                                            dictionnaire.put("Surname", "Non défini par l'utilisateur");
                                        } else {
                                            dictionnaire.put("Surname", privateinfos.get("Surname"));
                                        }
                                        if (privateinfos.get("FitnessParkName") == null) {
                                            dictionnaire.put("FitnessParkName", "Non défini par l'utilisateur");
                                        } else {

                                            dictionnaire.put("FitnessParkName", privateinfos.get("FitnessParkName"));

                                        }
                                        if (privateinfos.get("FitnessParkSurname") == null) {
                                            dictionnaire.put("FitnessParkSurname", "Non défini par l'utilisateur");
                                        } else {

                                            dictionnaire.put("FitnessParkSurname", privateinfos.get("FitnessParkSurname"));

                                        }


                                        if (privateinfos.get("Code") == null) {
                                            dictionnaire.put("Code", "Non défini par l'utilisateur");
                                        } else {
                                            dictionnaire.put("Code", privateinfos.get("Code"));

                                        }

                                        if (privateinfos.get("CodeGMF") == null) {
                                            dictionnaire.put("CodeGMF", "Non défini par l'utilisateur");
                                        } else {
                                            dictionnaire.put("CodeGMF", privateinfos.get("CodeGMF"));

                                        }

                                        if (privateinfos.get("Direct Energie") == null) {
                                            dictionnaire.put("Direct Energie", "Non défini par l'utilisateur");
                                        } else {
                                            dictionnaire.put("Direct Energie", privateinfos.get("Direct Energie"));

                                        }

                                        if (privateinfos.get("LienParainage") == null) {
                                            dictionnaire.put("LienParrainage", "Non défini par l'utilisateur");

                                        } else {
                                            dictionnaire.put("LienParrainage", privateinfos.get("LienParainage"));

                                        }
                                        if (privateinfos.get("CodeUber") == null) {
                                            dictionnaire.put("CodeUber", "Non défini par l'utilisateur");
                                        } else {
                                            dictionnaire.put("CodeUber", privateinfos.get("CodeUber"));

                                        }
                                        if (privateinfos.get("Deliveroo") == null) {
                                            dictionnaire.put("Deliveroo", "Non défini par l'utilisateur");
                                        } else {
                                            dictionnaire.put("Deliveroo", privateinfos.get("Deliveroo"));

                                        }
                                        if (privateinfos.get("Bolt") == null) {
                                            dictionnaire.put("Bolt", "Non défini par l'utilisateur");
                                        } else {
                                            dictionnaire.put("Bolt", privateinfos.get("Bolt"));

                                        }
                                        if (privateinfos.get("MAX") == null) {
                                            dictionnaire.put("MAX", "Non défini par l'utilisateur");
                                        } else {
                                            dictionnaire.put("MAX", privateinfos.get("MAX"));

                                        }
                                        if (privateinfos.get("Bourse Direct") == null) {
                                            dictionnaire.put("Bourse Direct", "Non défini par l'utilisateur");
                                        } else {
                                            dictionnaire.put("Bourse Direct", privateinfos.get("Bourse Direct"));

                                        }

                                        if (privateinfos.get("American Express") == null) {
                                            dictionnaire.put("American Express", "Non défini par l'utilisateur");
                                        } else {
                                            dictionnaire.put("American Express", privateinfos.get("American Express"));

                                        }

                                        if (privateinfos.get("AXAMail") == null) {
                                            dictionnaire.put("AXAMail", "Non défini par l'utilisateur");
                                        } else {
                                            dictionnaire.put("AXAMail", privateinfos.get("AXAMail"));

                                        }

                                        if (privateinfos.get("SaveurBiere") == null) {
                                            dictionnaire.put("Saveur Bière", "Non défini par l'utilisateur");
                                        } else {
                                            dictionnaire.put("Saveur Bière", privateinfos.get("SaveurBiere"));

                                        }

                                        if (privateinfos.get("Mail") == null) {
                                            dictionnaire.put("email", "Non défini par l'utilisateur");

                                        } else {
                                            dictionnaire.put("email", privateinfos.get("Mail"));

                                        }

                                        if (privateinfos.get("Ville") == null) {
                                            dictionnaire.put("Ville", "Non défini par l'utilisateur");

                                        } else {
                                            dictionnaire.put("Ville", privateinfos.get("Ville"));

                                        }

                                        if (privateinfos.get("Adresse") == null) {
                                            dictionnaire.put("Adresse", "Non défini par l'utilisateur");

                                        } else {
                                            dictionnaire.put("Adresse", privateinfos.get("Adresse"));

                                        }

                                        Log.i("dico:", dictionnaire.toString());


                                        List<Integer> counterlist = ParseUser.getCurrentUser().getList("Score");

                                        if (String_caught.equals("Boursorama")) {
                                            money = 110;

                                        }
                                        if (String_caught.equals("Fitness Park La Défense")) {
                                            money = 30;

                                        }

                                        if (String_caught.equals("ING")) {
                                            money = 80;

                                        }

                                        if (String_caught.equals("Revolut")) {
                                            money = 6;

                                        }

                                        if (String_caught.equals("Saveur Bière")) {
                                            money = 5;

                                        }
                                        if (String_caught.equals("BNP Paribas") | String_caught.equals("Société Générale") | String_caught.equals("Crédit Agricole")) {
                                            money = 30;

                                        }

                                        if (String_caught.equals("Uber")) {
                                            money = 10;

                                        }
                                        if (String_caught.equals("Deliveroo")) {
                                            money = 5;

                                        }
                                        if (String_caught.equals("Bolt")) {
                                            money = 7;

                                        }

                                        if (String_caught.equals("Bourse Direct")) {
                                            money = 170;

                                        }
                                        if (String_caught.equals("AXA")) {
                                            money = 75;

                                        }
                                        if (String_caught.equals("GMF")) {
                                            money = 30;

                                        }
                                        if (String_caught.equals("Direct Energie")) {
                                            money = 20;

                                        }
                                        Log.i("money", money.toString());


                                        if (counterlist != null) {
                                            if (counterlist.size() > 0) {

                                                Log.i("counterlist", counterlist.toString());

                                                Integer sor = counterlist.get(counterlist.size() - 1) + money;
                                                counterlist.add(sor);


                                            }
                                        }

                                        if (counterlist == null) {

                                            ArrayList<Integer> lip = new ArrayList<Integer>();
                                            lip.add(money);
                                            counterlist = lip;
                                        }

                                        if (counterlist != null) {
                                            if (counterlist.size() == 0) {

                                                ArrayList<Integer> lip = new ArrayList<Integer>();
                                                lip.add(money);
                                                counterlist = lip;
                                            }
                                        }

                                        ParseUser.getCurrentUser().put("Score", counterlist);

                                        Date currentTime = Calendar.getInstance().getTime();


                                        Calendar calendar = Calendar.getInstance();
                                        String month = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
                                        Log.i("month", month);


                                        List<String> DateList = ParseUser.getCurrentUser().getList("DateTime");
                                        if (DateList == null) {
                                            ArrayList<String> dater = new ArrayList<String>();
                                            dater.add(currentTime.toString());
                                            DateList = dater;
                                        } else {
                                            DateList.add(currentTime.toString());
                                        }
                                        ParseUser.getCurrentUser().put("DateTime", DateList);

                                        ParseUser.getCurrentUser().saveInBackground();


                                        receiverdico.add(dictionnaire);

                                        Log.i("receiver_dico", receiverdico.toString());

                                        Log.i("name to catch 1", name_to_catch);


                                        ParseUser userfound = objects.get(randomnumber);

                                        Map<String, HashMap<String, List<String>>> receiver_informations = ParseUser.getCurrentUser().getMap("ReceiverInfo");


                                        if (receiver_informations != null) {

                                            Log.i("receiveris ", "not null");
                                            if (receiver_informations.containsKey(userfound.getUsername())) {

                                                Log.i("user already in dict", userfound.getUsername());
                                                for (String key : receiver_informations.keySet()) {


                                                    if (key.equals(userfound.getUsername())) {

                                                        Log.i("selectiong user", userfound.getUsername());
                                                        if (receiver_informations.get(key).containsKey(String_caught)) {

                                                            for (String bank : receiver_informations.get(key).keySet()) {

                                                                if (bank.equals(String_caught)) {

                                                                    String actualdate = Calendar.getInstance().getTime().toString();
                                                                    receiver_informations.get(userfound.getUsername()).get(bank).add(actualdate);


                                                                    ParseUser.getCurrentUser().put("ReceiverInfo", receiver_informations);

                                                                    ParseUser.getCurrentUser().saveInBackground();


                                                                }


                                                            }
                                                        } else {

                                                            Log.i("dont contain", "String caught");
                                                            List<String> llist = new ArrayList<String>();
                                                            String actualdate = Calendar.getInstance().getTime().toString();
                                                            llist.add(actualdate);


                                                            receiver_informations.get(userfound.getUsername()).put(String_caught, llist);

                                                            ParseUser.getCurrentUser().put("ReceiverInfo", receiver_informations);

                                                            ParseUser.getCurrentUser().saveInBackground();

                                                        }


                                                    }

                                                }
                                            } else {

                                                List<String> llist = new ArrayList<String>();
                                                String actualdate = Calendar.getInstance().getTime().toString();
                                                llist.add(actualdate);
                                                HashMap<String, List<String>> hm = new HashMap();
                                                hm.put(String_caught, llist);


                                                receiver_informations.put(userfound.getUsername(), hm);


                                                ParseUser.getCurrentUser().put("ReceiverInfo", receiver_informations);

                                                ParseUser.getCurrentUser().saveInBackground();


                                            }


                                        }
                                        if (receiver_informations == null) {


                                            List<String> llist = new ArrayList<String>();
                                            String actualdate = Calendar.getInstance().getTime().toString();
                                            llist.add(actualdate);
                                            HashMap<String, List<String>> hm = new HashMap();
                                            hm.put(String_caught, llist);
                                            Map m = new ArrayMap();
                                            m.put(userfound.getUsername(), hm);


                                            ParseUser.getCurrentUser().put("ReceiverInfo", m);

                                            ParseUser.getCurrentUser().saveInBackground();


                                        }


                                        ParseUser.getCurrentUser().saveInBackground();


                                        List<Integer> counterlist2 = userfound.getList("Score");

                                        if (String_caught.equals("Boursorama")) {
                                            money = 110;

                                        }

                                        if (String_caught.equals("ING")) {
                                            money = 80;

                                        }

                                        if (String_caught.equals("Bourse Direct")) {
                                            money = 170;

                                        }

                                        if (String_caught.equals("Revolut")) {
                                            money = 6;

                                        }
                                        if (String_caught.equals("Fitness Park La Défense")) {
                                            money = 30;

                                        }
                                        if (String_caught.equals("Saveur Bière")) {
                                            money = 5;

                                        }
                                        if (String_caught.equals("BNP Paribas") | String_caught.equals("Société Générale") | String_caught.equals("Crédit Agricole")) {
                                            money = 30;

                                        }

                                        if (String_caught.equals("Uber")) {
                                            money = 10;

                                        }
                                        if (String_caught.equals("Deliveroo")) {
                                            money = 5;

                                        }
                                        if (String_caught.equals("GMF")) {
                                            money = 30;

                                        }
                                        if (String_caught.equals("Direct Energie")) {
                                            money = 20;

                                        }
                                        if (String_caught.equals("Bolt")) {
                                            money = 7;

                                        }

                                        if (String_caught.equals("MAX")) {
                                            money = 15;

                                        }


                                        if (counterlist2 != null) {
                                            counterlist2.add(counterlist2.get(counterlist2.size() - 1) + (int) Math.round(0.8 * money));

                                        } else {

                                            ArrayList<Integer> lip2 = new ArrayList<Integer>();
                                            lip2.add((int) Math.round(0.8 * money));
                                            counterlist2 = lip2;
                                        }

                                        userfound.put("Score", counterlist2);

                                        Log.i("Score", counterlist2.toString());


                                        Intent newintent = new Intent(getApplicationContext(), Reciever_Page.class);

                                        Log.i("Surname", dictionnaire.get("Surname"));
                                        Log.i("Name", dictionnaire.get("Name"));

                                        newintent.putExtra("Surname", dictionnaire.get("Surname"));
                                        newintent.putExtra("Name", dictionnaire.get("Name"));
                                        newintent.putExtra("MAX", dictionnaire.get("MAX"));
                                        newintent.putExtra("Username", dictionnaire.get("Username"));
                                        newintent.putExtra("LienParrainage", dictionnaire.get("LienParrainage"));
                                        newintent.putExtra("Code", dictionnaire.get("Code"));
                                        newintent.putExtra("Bank", String_caught);
                                        newintent.putExtra("email", dictionnaire.get("email"));
                                        newintent.putExtra("FortuneoLink", dictionnaire.get("FortuneoLink"));
                                        newintent.putExtra("CodeUber", dictionnaire.get("CodeUber"));
                                        newintent.putExtra("Deliveroo", dictionnaire.get("Deliveroo"));
                                        newintent.putExtra("Ville", dictionnaire.get("Ville"));
                                        newintent.putExtra("Bolt", dictionnaire.get("Bolt"));
                                        newintent.putExtra("Adresse", dictionnaire.get("Adresse"));
                                        newintent.putExtra("Bourse Direct", dictionnaire.get("Bourse Direct"));
                                        newintent.putExtra("American Express", dictionnaire.get("American Express"));
                                        newintent.putExtra("FitnessParkSurname", dictionnaire.get("FitnessParkSurname"));
                                        newintent.putExtra("FitnessParkName", dictionnaire.get("FitnessParkName"));

                                        Integer Counter = ParseUser.getCurrentUser().getInt("Counter");
                                        if (Counter != null) {

                                            Counter = Counter + 1;
                                        } else {

                                            Counter = 1;
                                        }

                                        ParseUser.getCurrentUser().put("Counter", Counter);
                                        ParseUser.getCurrentUser().saveInBackground();

                                        String p = "Code";

                                        if (dictionnaire.get("Bank").equals("ING")) {

                                            p = "Code";
                                        } else if (dictionnaire.get("Bank").equals("Revolut")) {

                                            p = "FortuneoLink";
                                        }
                                        else if (dictionnaire.get("Bank").equals("SaveurBiere")) {

                                            p = "SaveurBiere";
                                        }
                                        else {

                                            p = "LienParainage";
                                        }

                                        Util.Upload_Dictionary("ReceptionMenu", String_caught, userfound, dictionnaire, p);


                                        startActivity(newintent);


                                    } else {
                                        Toast.makeText(getApplicationContext(), "Les utilisateurs présents ont atteint leur quota de parrainnage", Toast.LENGTH_LONG).show();
                                    }
                                }

                                if (objects.size() == 0 & e == null) {
                                    Toast.makeText(SponsorActivity.this, "Pas de filleuls disponibles , attendez un peu que la base se remplisse", Toast.LENGTH_LONG).show();

                                }

                                if (e != null) {

                                    Toast.makeText(SponsorActivity.this, e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                }


                            }
                        }

                    });
                }
                return false;
            }


        });



        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        //client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void fillData() {
        Institutions = new ArrayList<>();

        Banks_and_Insurances = new HashMap<>();

        Institutions.add("Banques");
        Institutions.add("Assurances");
        Institutions.add("Services");
        Institutions.add("Loisirs");

        List<String> Banks = new ArrayList<>();

        Banks.addAll(Arrays.asList("ING", "Boursorama", "Revolut","MAX","Bourse Direct","American Express"));
                    /*    "BNP Paribas", "Crédit Agricole","Société Générale"));*/

        List<String> Insurances = new ArrayList<>();

        Insurances.addAll(Arrays.asList("AXA","GMF"));


        List<String> Leisure = new ArrayList<>();

        Leisure.addAll(Arrays.asList("Fitness Park La Défense","Saveur Bière"));

        List<String> Services = new ArrayList<>();

        Services.addAll(Arrays.asList("Uber","Bolt","Direct Energie","Deliveroo"));

        Banks_and_Insurances.put(Institutions.get(0), Banks);

        Banks_and_Insurances.put(Institutions.get(1), Insurances);

        Banks_and_Insurances.put(Institutions.get(2), Services);

        Banks_and_Insurances.put(Institutions.get(3), Leisure);

    }

  /*  *//**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     *//*
    public NotificationCompat.Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("MainScreen Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new NotificationCompat.Action.Builder(NotificationCompat.Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(NotificationCompat.Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }*/
}
