package com.serge_app.bethebanker;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.parse.ParseUser;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

//import android.support.v7.app.AppCompatActivity;


public class GraphActivity extends AppCompatActivity {
    //implements OnChartGestureListener,OnChartValueSelectedListener

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

       /* ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);*/

        MenuInflater menuInflater = new MenuInflater(this);

        menuInflater.inflate(R.menu.bethebankermenu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {



        if(item.getItemId()==R.id.MainPage)
        {

            Intent graphintent = new Intent(this,ActivityChoice.class);
            startActivity(graphintent);

        }

        if(item.getItemId()==R.id.Disclaimer)
        {
            Intent graphintent = new Intent(this,PolitiqueConfidentalite.class);
            startActivity(graphintent);

        }

        if(item.getItemId()==R.id.Classement)
        {
            Intent scrollList = new Intent(this,ScrollListActivity.class);
            startActivity(scrollList);


        }

        if(item.getItemId() == R.id.Logout) {

            ParseUser.logOut();

            Intent intent = new Intent(getApplicationContext(),MainActivity.class);

            startActivity(intent);

        }

        if(item.getItemId() == R.id.Destroy) {

            Intent intent = new Intent(getApplicationContext(),DestroyUser.class);

            startActivity(intent);

        }

        if(item.getItemId() == R.id.RecapReceive) {

            Intent intent = new Intent(getApplicationContext(),RecapMenu.class);

            startActivity(intent);

        }


        if(item.getItemId() == R.id.UploadMail) {

            Intent intent = new Intent(getApplicationContext(),UpdateMail.class);

            startActivity(intent);

        }



        return super.onOptionsItemSelected(item);
    }



    public void comp_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), ScrollListActivity.class);
        startActivity(graphintent);

    }

    public void graph_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), GraphActivity.class);
        startActivity(graphintent);

    }

    public void settings_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), UpdateMail.class);
        startActivity(graphintent);

    }

    public void mail_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), RecapMenu.class);
        startActivity(graphintent);

    }

    public void backarrow(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), ActivityChoice.class);
        startActivity(graphintent);

    }



    private static final String TAG = "GraphActivity";

    private LineChart mChart;
    private GraphView graphview;
    private Long intdate;
    Integer scorer;
    Integer minscore = 0;
    Integer limit ;
    Integer lab = 6;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        graphview = (GraphView) findViewById(R.id.Graph);

        LineGraphSeries series = new LineGraphSeries<DataPoint>();

        List<String> datelist = ParseUser.getCurrentUser().getList("DateTime");

        List<Integer> scorelist = ParseUser.getCurrentUser().getList("Score");

        if(datelist == null )
        {

            datelist = new ArrayList<String>();

            Date currentTime = Calendar.getInstance().getTime();

            datelist.add(currentTime.toString());



        }

        if(datelist.size()==0) {

            Date currentTime2 = Calendar.getInstance().getTime();

            datelist.add(currentTime2.toString());

        }

        if(scorelist==null)
        {

            scorelist = new ArrayList<Integer>();

            scorelist.add(0);

        }

        if(scorelist.size()==0) {


            scorelist.add(0);
        }



        final SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd", Locale.FRENCH);

        ArrayList<Long> longarray = new ArrayList<Long>();

        ArrayList<Date> datearray = new ArrayList<>();

        if ( datelist.size()>0) {

            Date created_at = ParseUser.getCurrentUser().getCreatedAt();



            DataPoint[] dp = new DataPoint[datelist.size() +1];
            limit = datelist.size()    ;

            //datearray.add(created_at);
            Long intedate = created_at.getTime();
            //longarray.add(intedate);

            if(limit >scorelist.size())
            {
                limit = scorelist.size();

            }


            //scorelist.add(0 ,0);







            for (int i = 0; i < limit; i++) {

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
                Log.i("date format", datelist.get(i));
                ParsePosition Position = new ParsePosition(0);
                Date dates = simpleDateFormat.parse(datelist.get(i), Position);
                datearray.add(dates);
                Long intdate = dates.getTime();
                longarray.add(intdate);
            }

           Collections.sort(datearray);


            dp[0] = new DataPoint(intedate, 0);

            series.appendData(dp[0], true, 100);


            for (int i = 0; i < limit ; i++) {

                Date dates = datearray.get(i);

                if (dates != null) {
                    Log.i("date", String.valueOf(dates.getTime()));
                    if(scorelist.get(i)==null & scorelist.size()==1)
                    {
                        scorer = 0;
                    }
                    if(scorelist.get(i) != null){

                        scorer = scorelist.get(i);
                    }
                    dp[i+1] = new DataPoint(dates, scorer);

                    series.appendData(dp[i+1], true, 100);

                }
            }





            }


        else{


        }






        series.setBackgroundColor(Color.argb(50, 0, 74, 148));

        series.setColor(Color.parseColor("#004a94"));

        series.setDrawBackground(true);

        graphview.setBackgroundColor(Color.rgb(255, 255, 255));

        series.setTitle("Progression de l'utilisateur "+ParseUser.getCurrentUser().getUsername());

        graphview.addSeries(series);



        graphview.getGridLabelRenderer().setHorizontalLabelsAngle(105);

        graphview.getLegendRenderer().setBackgroundColor(1);

        Integer Horlabel = 8;



        graphview.getGridLabelRenderer().setNumHorizontalLabels(Horlabel);



        //Log.i("maxscore",String.valueOf(Collections.max(scorelist)));
        Integer mscore = 0;

        if(scorelist.size()>0) {

            try {
                mscore = Collections.max(scorelist);
                Integer minscore = Collections.min(scorelist);
            }
            catch (Exception ClassCastException)
            {
                mscore = 0;
               Integer minscore = 0;

            }
        }

        Calendar cl = Calendar.getInstance();
        Long maxdate = cl.getTime().getTime();
        Long mindate = cl.getTime().getTime();

   if(longarray != null & longarray.size()>0)
   {

       maxdate = Collections.max(longarray);
       mindate = Collections.min(longarray);
   }


if( mscore == null)
{

    mscore = 0;
    Integer minscore = 0;
}
        graphview.getViewport().setMaxY(mscore);
        graphview.getViewport().setMinY(0);

        graphview.getViewport().setMaxX(maxdate);
       // graphview.getViewport().setMinX(mindate);
        //1.00002*

        graphview.getViewport().setXAxisBoundsManual(true);
        graphview.getGridLabelRenderer().setLabelsSpace(1);
        if(limit !=null) {
            if (limit<6)
            {

                Integer lab = limit;
            }
        }

        else
        {
            Integer lab = 6;
        }
        graphview.getGridLabelRenderer().setNumHorizontalLabels(lab);


        graphview.getViewport().setScrollable(true);

        graphview.getGridLabelRenderer().setHumanRounding(false);





        graphview.getLegendRenderer().setVisible(true);
        graphview.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);

        graphview.getGridLabelRenderer().setTextSize(30f);




        graphview.getGridLabelRenderer().reloadStyles();

        GridLabelRenderer gridLabel = graphview.getGridLabelRenderer();


        gridLabel.setLabelHorizontalHeight(200);
       gridLabel.setVerticalAxisTitle(" Estimation des Gains ");

        graphview.getGridLabelRenderer().setPadding(70);



        graphview.getViewport().setScrollable(true); // enables horizontal scrolling
        graphview.getViewport().setScrollableY(true); // enables vertical scrolling
        graphview.getViewport().setScalable(true); // enables horizontal zooming and scrolling
        graphview.getViewport().setScalableY(true); // enables vertical zooming and scrolling

        TextView text = findViewById(R.id.GraphText);

 if(mscore == 0)
 {
     String str =
         "<font color=##60689c><b> Vous n'avez encore rien recu !  </u></font>" ;


     text.setText(Html.fromHtml(str));
 }



            graphview.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {

                                                                   @Override
                                                                   public String formatLabel(double value, boolean isValuex) {
                                                                       if (isValuex) {

                                                                           return sdf.format(new Date((long) value));
                                                                       }

                                                                       return super.formatLabel(value, isValuex);
                                                                   }


                                                               }


            );
        }









    public class IndexAxisValueFormatter extends ValueFormatter
    {
        private String[] mValues = new String[] {};
        private int mValueCount = 0;

        public IndexAxisValueFormatter(ArrayList<String> values) {
            if (values != null)
                setValues(values.toArray(new String[values.size()]));
        }

        public IndexAxisValueFormatter(String[] values) {

            this.mValues = values;

        }


        /**
         * Constructor that specifies axis labels.
         *
         * @param values The values string array
         */
        public IndexAxisValueFormatter(List<String> values) {
            if (values != null)

                Log.i("values",values.toString());

            Log.i("al",values.toArray(new String[values.size()]).toString());
            setValues(values.toArray(new String[values.size()]));
        }




        @Override
        public String getFormattedValue(float value, AxisBase axis){

            return mValues[(int) value];
        }


        public String[] getValues()
        {
            return mValues;
        }

        public void setValues(String[] values)
        {
            if (values == null)
                values = new String[] {};

            this.mValues = values;
            this.mValueCount = values.length;
        }
    }




}

