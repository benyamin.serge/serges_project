package com.serge_app.bethebanker;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;
import uk.co.deanwild.materialshowcaseview.ShowcaseTooltip;

//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.CardView;


public class ActivityChoice extends AppCompatActivity {

    String id;
    Integer money;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

       /* ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);*/

        MenuInflater menuInflater = new MenuInflater(this);

        menuInflater.inflate(R.menu.bethebankermenu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {



        if(item.getItemId()==R.id.MainPage)
        {

            Intent graphintent = new Intent(this,ActivityChoice.class);
            startActivity(graphintent);

        }

        if(item.getItemId()==R.id.Disclaimer)
        {
            Intent graphintent = new Intent(this,PolitiqueConfidentalite.class);
            startActivity(graphintent);

        }

        if(item.getItemId()==R.id.Classement)
        {
            Intent scrollList = new Intent(this,ScrollListActivity.class);
            startActivity(scrollList);


        }

        if(item.getItemId()==R.id.GraphPage)
        {

            Intent graphintent = new Intent(this,GraphActivity.class);
            startActivity(graphintent);

        }

        if(item.getItemId() == R.id.Logout) {

            ParseUser.logOut();

            Intent intent = new Intent(getApplicationContext(),MainActivity.class);

            startActivity(intent);

        }

        if(item.getItemId() == R.id.Destroy) {

            Intent intent = new Intent(getApplicationContext(),DestroyUser.class);

            startActivity(intent);

        }

        if(item.getItemId() == R.id.RecapReceive) {

            Intent intent = new Intent(getApplicationContext(),RecapMenu.class);

            startActivity(intent);

        }

        if(item.getItemId() == R.id.UploadMail) {

            Intent intent = new Intent(getApplicationContext(), UpdateMail.class);

            startActivity(intent);
        }


        return super.onOptionsItemSelected(item);
    }


    public void Sponsor(View view)
    {

        Intent graphintent = new Intent(this, SponsorActivity.class);
        startActivity(graphintent);


    }

    public void BeSponsored(View view)
    {

        Intent graphintent = new Intent(this, BeSponsoredActivity.class);
        startActivity(graphintent);


    }


    public void comp_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), ScrollListActivity.class);
        startActivity(graphintent);

    }

    public void graph_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), GraphActivity.class);
        startActivity(graphintent);

    }

    public void settings_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), UpdateMail.class);
        startActivity(graphintent);

    }

    public void mail_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), RecapMenu.class);
        startActivity(graphintent);

    }

    public void backarrow(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), ActivityChoice.class);
        startActivity(graphintent);

    }

    private void presentShowcaseSequence() {

        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(500); // half second between each showcase view

        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this, SHOWCASE_ID);

        sequence.setOnItemShownListener(new MaterialShowcaseSequence.OnSequenceItemShownListener() {
            @Override
            public void onShow(MaterialShowcaseView itemView, int position) {
                //Toast.makeText(itemView.getContext(), "Item #" + position, Toast.LENGTH_SHORT).show();
            }
        });

        ImageView settings = findViewById(R.id.settings);
        ImageView mail = findViewById(R.id.logomail);
        ImageView logograph = findViewById(R.id.logographe);
        ImageView logocomp = findViewById(R.id.logocomp);
        CardView cardview = findViewById(R.id.card_view);
        CardView cardview2 = findViewById(R.id.card_view2);

        sequence.setConfig(config);

        Integer corner = 50;
        //#007686

        ShowcaseTooltip toolTip1 = ShowcaseTooltip.build(this)
                .corner(corner)
                .textColor(Color.parseColor("#FFFFFF"))
                .color(Color.parseColor("#2F4172"))
                .text(" <big><b>J'ai compris </b></big>");

        ShowcaseTooltip toolTip2 = ShowcaseTooltip.build(this)
                .corner(corner)
                .textColor(Color.parseColor("#FFFFFF"))
                .color(Color.parseColor("#2F4172"))
                .text(" <big><b>J'ai compris </b></big>");

        ShowcaseTooltip toolTip3 = ShowcaseTooltip.build(this)
                .corner(corner)
                .textColor(Color.parseColor("#2F4172"))
                .text(" <big><b>J'ai compris </b></big>");

        ShowcaseTooltip toolTip4 = ShowcaseTooltip.build(this)
                .corner(corner)
                .textColor(Color.parseColor("#2F4172"))
                .text(" <big><b>J'ai compris </b></big>");

        ShowcaseTooltip toolTip5 = ShowcaseTooltip.build(this)
                .corner(corner)
                .textColor(Color.parseColor("#FFFFFF"))
                .color(Color.parseColor("#2F4172"))
                .text(" <big><b>J'ai compris </b></big>");

        ShowcaseTooltip toolTip6 = ShowcaseTooltip.build(this)
                .corner(corner)
                .textColor(Color.parseColor("#FFFFFF"))
                .color(Color.parseColor("#2F4172"))
                .text(" <big><b>J'ai compris </b></big>");


        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)

                        .setTarget(settings)
                        .setShapePadding(12)

                        .setContentText("Renseignez votre mail pour être alerté lorsque l'on utilise votre code de parrainage")
                        .withCircleShape()
                        .setToolTip(toolTip1)
                        .setTooltipMargin(20)

                        .setDismissOnTouch(true)

                        .singleUse(SHOWCASE_ID)

                        .setMaskColour(Color.parseColor("#EDFFFFFF"))
                        .setContentTextColor(Color.parseColor("#004a94"))
                        //.setDismissTextColor(Color.parseColor("#004a94"))
                        .build()
        );

        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)

                        .setTarget(mail)
                        .setShapePadding(12)
                        //.setDismissText("J'ai compris")
                        .setContentText("Cochez les cases correspondantes aux codes de parrainage utilisés pour notifier votre parrain ")
                        .withCircleShape()
                        .setToolTip(toolTip2)
                        .setTooltipMargin(20)

                        .setDismissOnTouch(true)

                        .singleUse(SHOWCASE_ID)

                        .setMaskColour(Color.parseColor("#EDFFFFFF"))
                        .setContentTextColor(Color.parseColor("#004a94"))
                        //.setDismissTextColor(Color.parseColor("#004a94"))
                        .build()
        );

        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)

                        .setTarget(cardview)

                        .singleUse(SHOWCASE_ID)
                        .setToolTip(toolTip3)
                        .setTooltipMargin(20)
                        .setDismissOnTouch(true)


                        .setContentText("Cliquez sur l'encadré ci-dessus pour trouver votre parrain")
                        .withRectangleShape()
                        .build()
        );
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(cardview2)


                        .setToolTip(toolTip4)
                        .setTooltipMargin(20)
                        .setDismissOnTouch(true)

                        .singleUse(SHOWCASE_ID)
                        .setContentText("Cliquez sur l'encadré ci-dessus pour trouver votre filleul")
                        .withRectangleShape()
                        .build()
        );
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setSkipText("Passer")
                        .setTarget(logograph)
                        .setShapePadding(12)

                        .singleUse(SHOWCASE_ID)
                        .setContentText("Contemplez vos gains !")
                        .withCircleShape()
                        .setMaskColour(Color.parseColor("#EDFFFFFF" ))
                        .setToolTip(toolTip5)
                        .setTooltipMargin(20)
                        .setDismissOnTouch(true)

                        .setContentTextColor(Color.parseColor("#004a94"))
                        .setDismissTextColor(Color.parseColor("#004a94"))

                        .build()
        );
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setSkipText("Passer")

                        .setTarget(logocomp)
                        .setShapePadding(12)

                        .setContentText("Mesurez vous aux autres utilisateurs et devenez le meilleur !")
                        .withCircleShape()
                        .setTooltipMargin(20)
                        .setDismissOnTouch(true)
                        .setToolTip(toolTip6)
                        .singleUse(SHOWCASE_ID)
                        .setTooltipMargin(20)
                        .setMaskColour(Color.parseColor("#EDFFFFFF"))
                        .setContentTextColor(Color.parseColor("#004a94"))
                        .setDismissTextColor(Color.parseColor("#004a94"))
                        .build()
        );

        sequence.start();

    }


    private static final String SHOWCASE_ID = "En prod20" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice);


        presentShowcaseSequence();

/*        SharedPreferences prefs = getSharedPreferences("UserData", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        if(! prefs.contains("Tutorial")) {


            presentShowcaseSequence();
            editor.putString("Tutorial", "Tuto");
            editor.commit();
        }*/

        MobileAds.initialize(getApplicationContext(),"ca-app-pub-3940256099942544~3347511713");

  /*      MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });*/


        AdView mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

/*        ImageView imgButtonpromo = (ImageView) findViewById(R.id.image_promo);
        imgButtonpromo.setImageResource(R.drawable.arrow);*/

        ImageView imgButton = (ImageView) findViewById(R.id.image_parrainer);
        imgButton.setImageResource(R.drawable.arrow);

        ImageView besponsored = (ImageView) findViewById(R.id.image_etre_parraine);
        besponsored.setImageResource(R.drawable.arrow);

        final List user_receiver = new ArrayList();

        final List<String> dates = new ArrayList<String>();

        final List<Integer> scorerr = new ArrayList<Integer>();

        ParseQuery<ParseUser> query = ParseUser.getQuery();

        //query.whereContains("ReceiverInfo",user.getUsername());

        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> objects, ParseException e) {

                if (objects.size() > 0) {

                    for (ParseUser ob : objects) {

                        if (ob.getMap("ValidatedCode") != null) {

                            Log.i("VAL CODE", ob.getMap("ValidatedCode").toString());

                            Log.i("KEY7", ob.getMap("ValidatedCode").keySet().toString());


                            if (ob.getMap("ValidatedCode").containsKey(ParseUser.getCurrentUser().getUsername())) {

                                Log.i("user contains info", ob.getUsername());

                                Map<String, Map> receiverInfo = ob.getMap("ValidatedCode");

                                Map<String, List<String>> bankndate = receiverInfo.get(ParseUser.getCurrentUser().getUsername());



                                Log.i("bankanddate", bankndate.toString());



                                user_receiver.add(bankndate);




                                for (String keyy : bankndate.keySet()) {


                                    if (keyy.equals("Boursorama")) {
                                        money = 110;

                                    }
                                    if (keyy.equals("Direct Energie")) {
                                        money = 30;

                                    }

                                    if (keyy.equals("ING")) {
                                        money = 80;

                                    }

                                    if (keyy.equals("Revolut")) {
                                        money = 6;

                                    }

                                    if (keyy.equals("AXA")) {
                                        money = 75;

                                    }
                                    if (keyy.equals("BNP Paribas") | keyy.equals("Société Générale") | keyy.equals("Crédit Agricole")) {
                                        money = 30;

                                    }

                                    if (keyy.equals("Uber")  ) {
                                        money = 10;

                                    }
                                    if (keyy.equals("Deliveroo")  ) {
                                        money = 5;

                                    }
                                    if (keyy.equals("Bolt")  ) {
                                        money = 10;

                                    }
                                    if (keyy.equals("Saveur Bière")  ) {
                                        money = 5;

                                    }
                                    if (keyy.equals("MAX")  ) {
                                        money = 5;
                                    }
                                    if (keyy.equals("Fitness Park La Défense")  ) {
                                        money = 30;
                                    }

                                    if (keyy.equals("Bourse Direct")  ) {
                                        money = 20;

                                    }
                                    if (keyy.equals("American Express")  ) {
                                        money = 50;

                                    }


                                    List<String> l = bankndate.get(keyy);

                                    Log.i("list date", l.toString());

                                    //dates.add(l.toString());

                                    for (String date : l) {

                                        Log.i("date", date.toString());

                                        dates.add(date);

                                        if (scorerr != null)
                                            if (scorerr.isEmpty() == false) {
                                                Log.i("scorrer not null", " not null");
                                                scorerr.add(scorerr.get(scorerr.size() - 1) + money);
                                            } else {
                                                scorerr.add(money);
                                                Log.i("scorrer null", "null");
                                            }


                                    }

                                    ParseUser.getCurrentUser().put("Score", scorerr);

                                    ParseUser.getCurrentUser().put("DateTime", dates);


                                    ParseUser.getCurrentUser().saveInBackground();


                                }





                                Log.i("bankanddatevalues", dates.toString());


                                // List<Integer> score = ParseUser.getCurrentUser().getList("Score");


                                Log.i("userreceiver", user_receiver.toString());

                                ParseUser.getCurrentUser().put("ReceiverTime", user_receiver);

                                ParseUser.getCurrentUser().put("DateTime", dates);

                                ParseUser.getCurrentUser().put("Score", scorerr);


                                ParseUser.getCurrentUser().saveInBackground();


                            }


                        }


                    }
                } else {

                    //Log.i("no values","shit");
                }

            }


        });






    }
}
