package com.serge_app.bethebanker;

/**
 * Created by WFTD- on 11/05/2019.
 */

public class Person {
    String Name;
    Integer Score;
    String logoImage;
    Integer positionscore;

    public Person(String Name , Integer Score,String logoImage , Integer positionscore)
    {
        this.Name = Name;
        this.Score = Score;
        this.logoImage = logoImage;
        this.positionscore = positionscore;


    }

    public String getName()
    {

        return this.Name;
    }

    public Integer getScore()
    {

        return this.Score;
    }

    public Integer getPositionscore()
    {

        return this.positionscore;
    }

    public String getUri()
    {

        return this.logoImage;
    }



}
