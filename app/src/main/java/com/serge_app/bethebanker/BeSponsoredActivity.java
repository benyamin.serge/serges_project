package com.serge_app.bethebanker;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.ArrayMap;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.api.GoogleApiClient;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;
import uk.co.deanwild.materialshowcaseview.ShowcaseTooltip;

import static com.parse.ParseUser.getCurrentUser;

//import android.support.v7.app.ActionBar;


public class BeSponsoredActivity extends AppCompatActivity implements View.OnKeyListener, View.OnClickListener, DialogInterface.OnKeyListener {

    String id;
    Integer money;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    @Override
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i == keyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return false;
    }

    @Override
    public boolean onKey(DialogInterface dialog, int i, KeyEvent keyEvent) {
        if (i == keyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return false;
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.background_layout )

        {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        }


    }


    Integer valmax;
    Integer randomnumber;
    Integer ppd = 0;
    ExpandableListView expandableListView;

    String parrain_choisi;


    List<String> Institutions;


    Map<String, List<String>> Banks_and_Insurances;

    ExpandableListAdapter listAdapter;

    ArrayList<String> Friends_List = new ArrayList();

    final Map<String, String> dictionnaire = new HashMap<String, String>();

    final List<Map<String, String>> receiverdico = new ArrayList<Map<String, String>>();

    String name_to_catch = "";

    ParseUser user_got = ParseUser.getCurrentUser();

    String usernamefound = " ";

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

                /* ParseACL defaultACL = new ParseACL();
                defaultACL.setPublicReadAccess(true);
                defaultACL.setPublicWriteAccess(true);
                ParseACL.setDefaultACL(defaultACL, true);*/

        MenuInflater menuInflater = new MenuInflater(this);

        menuInflater.inflate(R.menu.bethebankermenu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == R.id.GraphPage) {

            Intent graphintent = new Intent(this, GraphActivity.class);
            startActivity(graphintent);

        }

        if (item.getItemId() == R.id.Disclaimer) {
            Intent graphintent = new Intent(this, PolitiqueConfidentalite.class);
            startActivity(graphintent);

        }

        if (item.getItemId() == R.id.Classement) {
            Intent scrollList = new Intent(this, ScrollListActivity.class);
            startActivity(scrollList);


        }

        if (item.getItemId() == R.id.Logout) {

            ParseUser.logOut();

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);

            startActivity(intent);

        }

        if (item.getItemId() == R.id.Destroy) {

            Intent intent = new Intent(getApplicationContext(), DestroyUser.class);

            startActivity(intent);

        }

        if (item.getItemId() == R.id.RecapReceive) {

            Intent intent = new Intent(getApplicationContext(), RecapMenu.class);

            startActivity(intent);

        }
        if (item.getItemId() == R.id.UploadMail) {

            Intent intent = new Intent(getApplicationContext(), UpdateMail.class);

            startActivity(intent);

        }

        if (item.getItemId() == R.id.MainPage) {

            Intent graphintent = new Intent(this, ActivityChoice.class);
            startActivity(graphintent);

        }

        return super.onOptionsItemSelected(item);
    }



    public void comp_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), ScrollListActivity.class);
        startActivity(graphintent);

    }

    public void graph_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), GraphActivity.class);
        startActivity(graphintent);

    }

    public void settings_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), UpdateMail.class);
        startActivity(graphintent);

    }

    public void mail_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), RecapMenu.class);
        startActivity(graphintent);

    }

    public void backarrow(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), ActivityChoice.class);
        startActivity(graphintent);

    }

    private void presentShowcaseSequence() {

        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(500); // half second between each showcase view

        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this, SHOWCASE_ID);

        sequence.setOnItemShownListener(new MaterialShowcaseSequence.OnSequenceItemShownListener() {
            @Override
            public void onShow(MaterialShowcaseView itemView, int position) {
                //Toast.makeText(itemView.getContext(), "Item #" + position, Toast.LENGTH_SHORT).show();
            }
        });

        //ImageView settings = findViewById(R.id.settings);
        ImageView mail = findViewById(R.id.logomail);
                //findViewById(R.id.logomail);


        sequence.setConfig(config);

        Integer corner = 50;
        ShowcaseTooltip toolTip1 = ShowcaseTooltip.build(this)
                .corner(corner)
                .textColor(Color.parseColor("#2F4172"))
                .color(Color.parseColor("#FFFFFF"))
                .text(" <big><b>J'ai compris </b></big>");



        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(mail)
                        .withRectangleShape()
                        .setTooltipMargin(20)
                        .setShapePadding(1)
                        .setDismissOnTouch(true)
                        .setToolTip(toolTip1)
                        .singleUse(SHOWCASE_ID)
                        .setContentText("A chaque utilisation ,un jeton est compté pour votre parrain ." +
                                " Il  pourra parrainer jusqu'à la limite autorisée par la compagnie : " +
                                "utilisez la fonctionnalité à bon escient pour le bon fonctionnement de l'application ")
                        //.withCircleShape()
                       // .setTooltipMargin(1)
                      //  .setShapePadding(0)
                        .setMaskColour(Color.parseColor("#2F4172"))
                        .setContentTextColor(Color.parseColor("#ffffff"))
                        .setDismissTextColor(Color.parseColor("#ffffff"))
                        .build()
        );





        sequence.start();

    }
    private static final String SHOWCASE_ID = "SponsoredFirst_14";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_be_sponsored

        );

        presentShowcaseSequence();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Trouve ton Parrain");

        RelativeLayout bckgound = (RelativeLayout) findViewById(R.id.background_layout);




        bckgound.setOnClickListener(BeSponsoredActivity.this);

        ImageView imageButton = (ImageView) findViewById(R.id.logoImage2);


        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent graphintent = new Intent(getApplicationContext(), ActivityChoice.class);
                startActivity(graphintent);


            }
        });




        expandableListView = (ExpandableListView) findViewById(R.id.ExpandableListView);
        fillData();

        listAdapter = new MyExpandableListAdapter(this, Institutions, Banks_and_Insurances);
        expandableListView.setAdapter(listAdapter);


        //TextView textView = (TextView) expandableListView.findViewById(R.id.ExpandableListView);

        // textView.setTextColor(/* some color */);

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {

                //Toast.makeText(MainScreen.this, Institutions.get(i) + " :" + Banks_and_Insurances.get(Institutions.get(i)).get(i1), Toast.LENGTH_LONG).show();

                final String String_caught = Banks_and_Insurances.get(Institutions.get(i)).get(i1);


                final ArrayList<String> liste_parrains = new ArrayList<String>();


                ParseQuery<ParseUser> querry = ParseUser.getQuery();

                querry.whereNotEqualTo("username", getCurrentUser().getUsername());

                querry.whereContains("Banks", String_caught);

                querry.findInBackground(new FindCallback<ParseUser>() {
                    @Override
                    public void done(List<ParseUser> objects, ParseException e) {

                        for (ParseUser ob : objects) {
                            Log.i("obbb", ob.getUsername().toString());
                            liste_parrains.add(ob.getUsername().toString());

                            Log.i("dada", liste_parrains.get(0));
                        }

                    }
                });


                EditText textt = new EditText(BeSponsoredActivity.this);


                AlertDialog.Builder builder = new AlertDialog.Builder(BeSponsoredActivity.this);
                builder.setTitle("Informations requises");

                if (liste_parrains != null) {
                            Log.i("liste parrains1", liste_parrains.toString());

                        } else {

                            Toast.makeText(getApplicationContext(), "Il n y 'a pas encore de parrains, attendez un peu", Toast.LENGTH_LONG).show();
                        }


                        if ((String_caught.equals("BNP Paribas")) | (String_caught.equals("Crédit Agricole")) | (String_caught.equals("Société Générale"))) {

                            //Log.i("ING str c",String_caught);

                            LayoutInflater BNPinflater = (LayoutInflater) BeSponsoredActivity.this.getSystemService(BeSponsoredActivity.this.LAYOUT_INFLATER_SERVICE);
                            //builder.setMessage("Renseignez vos details");
                            builder.setView(R.layout.bnp_view);
                            //In case it gives you an error for setView(View) try
                            final View BNPview = BNPinflater.inflate(R.layout.bnp_view, null);


                            builder.setView(BNPview);

                            //, 86, 60, 10, 0
                            final EditText SurnameUser = (EditText) BNPview.findViewById(R.id.SurnameUserBNP);
                            final EditText NameUser = (EditText) BNPview.findViewById(R.id.NameUserBNP);
                            final EditText Mail = (EditText) BNPview.findViewById(R.id.MailBNP);


                            builder.setPositiveButton("Envoyer les informations", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {


                                    Log.i("Surname", String.valueOf(!(SurnameUser.getText().toString().isEmpty())));


                                    if (!(SurnameUser.getText().toString().isEmpty())) {
                                        Log.i("Surname", SurnameUser.getText().toString());

                                        if (!(NameUser.getText().toString().isEmpty())) {

                                            if (!(Mail.getText().toString().isEmpty())) {

                                                HashMap<String,String> params = new HashMap<String, String>();

                                                params.put("username",ParseUser.getCurrentUser().getUsername());
                                                params.put("Name",NameUser.getText().toString());
                                                params.put("Surname",SurnameUser.getText().toString());
                                                params.put("Mail",Mail.getText().toString());

                                                ParseQuery<ParseObject> query = ParseQuery.getQuery("Banker");

                                                ParseCloud.callFunctionInBackground("BNPGroup",params,  new FunctionCallback<String>() {
                                                    public void done(String str, ParseException e) {
                                                        if (e == null) {
                                                            Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();
                                                        } else {
                                                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                                        }
                                                    }

                                                });


                                                List<String> BList = ParseUser.getCurrentUser().getList("Banks");


                                                if (BList == null) {

                                                    ArrayList<String> DList = new ArrayList<String>();
                                                    DList.add(String_caught);

                                                    ParseUser.getCurrentUser().put("Banks", DList);

                                                } else {
                                                    if (BList.contains(String_caught)) {
                                                        BList = BList;
                                                        ParseUser.getCurrentUser().put("Banks", BList);
                                                    } else {
                                                        Log.i("List doesn't contain:", String.valueOf(BList.contains(String_caught)));
                                                        BList.add(String_caught);
                                                        ParseUser.getCurrentUser().put("Banks", BList);
                                                    }
                                                }


                                                ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
                                                    @Override
                                                    public void done(ParseException e) {
                                                        if (e == null) {
                                                            Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();

                                                            Integer Counter = ParseUser.getCurrentUser().getInt("Counter");
                                                            if (Counter != null) {

                                                                Counter = Counter + 1;
                                                            } else {

                                                                Counter = 1;
                                                            }

                                                            ParseUser.getCurrentUser().put("Counter", Counter);
                                                            ParseUser.getCurrentUser().saveInBackground();

                                                        } else {
                                                            Toast.makeText(BeSponsoredActivity.this,
                                                                    "Le téléchargement des données n'a pas fonctionné et le message est" + e.getMessage(),
                                                                    Toast.LENGTH_LONG).show();

                                                        }
                                                    }
                                                });
                                            } else {

                                                Toast.makeText(getApplicationContext(), "S'il vous plait remplissez votre adresse mail", Toast.LENGTH_LONG).show();
                                            }

                                        } else {

                                            Toast.makeText(getApplicationContext(), "S'il vous plait remplissez votre Nom", Toast.LENGTH_LONG).show();
                                        }

                                    }
                                    if (SurnameUser.getText().toString().isEmpty()) {

                                        Toast.makeText(getApplicationContext(), "S'il vous plait remplissez votre Prénom", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });

                            builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            });

                            builder.show();
                            builder.create();

                        }


                        if (String_caught.equals("GMF") | String_caught.equals("Boursorama") | String_caught.equals("AXA") |String_caught.equals("Fitness Park La Défense") |
                                String_caught.equals("ING") | String_caught.equals("Revolut") |String_caught.equals("Direct Energie") |
                                String_caught.equals("Bolt")| String_caught.equals("Uber")|String_caught.equals("Deliveroo")|
                                String_caught.equals("Saveur Bière") |String_caught.equals("MAX")|
                                String_caught.equals("Bourse Direct") |String_caught.equals("American Express") ) {
                            ParseQuery<ParseUser> query = ParseUser.getQuery();


                            query.whereNotEqualTo("username", getCurrentUser().getUsername());

                            ArrayList<String> fake = new ArrayList<String>();
                            fake.add(String_caught);

                            query.whereNotContainedIn("Banks", fake);


                            final List<Map<String, String>> receiverdico = new ArrayList<Map<String, String>>();

                            query.findInBackground(new FindCallback<ParseUser>() {
                                @Override
                                public void done(List<ParseUser> objects, ParseException e) {


                                    if (e == null) {


                                        if (objects.size() > 0) {
                                            Random rand = new Random();

                                            final Map<String, String> dictionnaire = new HashMap<String, String>();


                                            Calendar actualtime = Calendar.getInstance();

                                            if (String_caught.equals("Boursorama")) {

                                                valmax = 3;

                                            }
                                            if (String_caught.equals("MAX")) {

                                                valmax = 10;


                                            }

                                            if (String_caught.equals("GMF")) {

                                                valmax = 5;


                                            }
                                            if (String_caught.equals("Direct Energie")) {

                                                valmax = 20;

                                            }
                                            if (String_caught.equals("Fitness Park La Défense")) {

                                                valmax = 1000;

                                            }

                                            if (String_caught.equals("AXA")) {

                                                valmax = 20;


                                            }

                                            if (String_caught.equals("Revolut")) {

                                                valmax = 100;


                                            }

                                            if (String_caught.equals("ING")) {
                                                valmax = 20;

                                            }

                                            if (String_caught.equals("Bourse Direct")) {
                                                valmax = 10;

                                            }


                                            if (String_caught.equals("Uber") | String_caught.equals("Saveur Bière") |String_caught.equals("Deliveroo") |
                                                    String_caught.equals("Bolt") | String_caught.equals("American Express")) {

                                                valmax = 1000;
                                            }

                                            Map<String, Map<String, List<String>>> R = ParseUser.getCurrentUser().getMap("ReceiverInfo");
                                            if (R != null) {

                                                Log.i("R", R.toString());
                                                for (String s : R.keySet()) {
                                                    if (liste_parrains.contains(s)) {

                                                        if (R.get(s).containsKey(String_caught)) {
                                                            liste_parrains.remove(s);


                                                        }

                                                    }

                                                }
                                            }
                                            for (String parrain : liste_parrains) {

                                                Integer pp = 0;


                                                for (int j = 0; j < objects.size(); j++) {



                                                    Map<String, Map<String, List<String>>> maplister = objects.get(j).getMap("ReceiverInfo");


                                                    if (maplister != null) {



                                                        if (maplister.containsKey(parrain)) {

                                                            Map<String, List<String>> datelsit = maplister.get(parrain);

                                                            //ArrayList<Long> transformed_dates = new ArrayList<Long>();
                                                            if (datelsit != null) {




                                                                for (String key : datelsit.keySet()) {

                                                                    if (key.equals(String_caught)) {

                                                                        Calendar calendar = Calendar.getInstance();

                                                                        if (datelsit.get(key) != null) {

                                                                            for (String dates : datelsit.get(key)) {


                                                                                //calendar.setTime(date);

                                                                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);

                                                                                ParsePosition Position = new ParsePosition(0);

                                                                                Date date = simpleDateFormat.parse(dates, Position);



                                                                                if (String_caught.equals("Boursorama")) {
                                                                                    long diff = (calendar.getTime().getTime() - date.getTime()) / (1000 * 3600 * 24);


                                                                                    if (diff < 32) {
                                                                                        pp = pp + 1;

                                                                                    }
                                                                                }

                                                                                if (String_caught.equals("ING") | String_caught.equals("Revolut") |
                                                                                        String_caught.equals("Bourse Direct") | String_caught.equals("Direct Energie") |
                                                                                        String_caught.equals("GMF") | String_caught.equals("MAX") | String_caught.equals("AXA")) {
                                                                                    long diff = (calendar.getTime().getTime() - date.getTime()) / (1000 * 3600 * 24);

                                                                                    if (diff < 366) {
                                                                                        pp = pp + 1;
                                                                                    }



                                                                                }

                                                                                if (String_caught.equals("Uber") | String_caught.equals("Saveur Bière")|String_caught.equals("Fitness Park La Défense") |
                                                                                        String_caught.equals("Bolt") |String_caught.equals("Deliveroo") |
                                                                                        String_caught.equals("American Express")) {

                                                                                    ppd = 0;
                                                                                    pp = 0;


                                                                                }


                                                                                if (pp >= valmax) {
                                                                                    break;
                                                                                }

                                                                            }
                                                                        }
                                                                        ppd = pp;
                                                                        if (ppd >= valmax) {
                                                                            break;
                                                                        }


                                                                    }
                                                                }


                                                            }


                                                        }


                                                    }


                                                }


                                                if (ppd != null) {


                                                    if (ppd >= valmax) {
                                                        parrain_choisi = null;

                                                    }

                                                    if (ppd < valmax) {

                                                        parrain_choisi = parrain;
                                                    }
                                                }


                                            }


                                            if (parrain_choisi == null) {


                                                Toast.makeText(BeSponsoredActivity.this, "Les utilisateurs présents ont atteint leur quota de parrainnage", Toast.LENGTH_LONG).show();

                                            }


                                            if (parrain_choisi != null) {


                                                name_to_catch = parrain_choisi;



                                                dictionnaire.put("Username", name_to_catch);

                                                HashMap<String, String> params = new HashMap<String, String>();
                                                params.put("user", name_to_catch);
                                                //request.put("user",ParseUser.getCurrentUser());



                                                HashMap<String, String> privateinfos = new HashMap<String, String>();
                                                ParseCloud.callFunctionInBackground("RequestPrivateInfos", params, new FunctionCallback<HashMap<String, String>>() {
                                                    public void done(HashMap<String, String> privateinfos, ParseException e) {
                                                        if (e == null) {


                                                            if (privateinfos.get("Name") == null) {
                                                                dictionnaire.put("Name", "Non défini par l'utilisateur");
                                                            } else {
                                                                dictionnaire.put("Name", privateinfos.get("Name"));

                                                            }
                                                            if (privateinfos.get("Surname") == null) {
                                                                dictionnaire.put("Surname", "Non défini par l'utilisateur");
                                                            } else {
                                                                dictionnaire.put("Surname", privateinfos.get("Surname"));

                                                            }
                                                            if (privateinfos.get("FitnessParkName") == null) {
                                                                dictionnaire.put("FitnessParkName", "Non défini par l'utilisateur");
                                                            } else {
                                                                dictionnaire.put("FitnessParkName", privateinfos.get("FitnessParkName"));

                                                            }
                                                            if (privateinfos.get("FitnessParkSurname") == null) {
                                                                dictionnaire.put("FitnessParkSurname", "Non défini par l'utilisateur");
                                                            } else {
                                                                dictionnaire.put("FitnessParkSurname", privateinfos.get("FitnessParkSurname"));

                                                            }


                                                            if (privateinfos.get("Code") == null) {
                                                                dictionnaire.put("Code", "Non défini par l'utilisateur");
                                                            } else {
                                                                dictionnaire.put("Code", privateinfos.get("Code"));

                                                            }
                                                            if (privateinfos.get("Bolt") == null) {
                                                                dictionnaire.put("Bolt", "Non défini par l'utilisateur");
                                                            } else {
                                                                dictionnaire.put("Bolt", privateinfos.get("Bolt"));

                                                            }

                                                            if (privateinfos.get("CodeUber") == null) {
                                                                dictionnaire.put("CodeUber", "Non défini par l'utilisateur");
                                                            } else {
                                                                dictionnaire.put("CodeUber", privateinfos.get("CodeUber"));

                                                            }
                                                            if (privateinfos.get("Deliveroo") == null) {
                                                                dictionnaire.put("Deliveroo", "Non défini par l'utilisateur");
                                                            } else {
                                                                dictionnaire.put("Deliveroo", privateinfos.get("Deliveroo"));

                                                            }
                                                            if (privateinfos.get("LienParainage") == null) {
                                                                dictionnaire.put("LienParrainage", "Non défini par l'utilisateur");

                                                            } else {
                                                                dictionnaire.put("LienParrainage", privateinfos.get("LienParainage"));

                                                            }
                                                            if (privateinfos.get("SaveurBiere") == null) {
                                                                Log.i("S"," B");
                                                                dictionnaire.put("SaveurBiere", "Non défini par l'utilisateur");

                                                            } else {
                                                                dictionnaire.put("SaveurBiere", privateinfos.get("SaveurBiere"));

                                                            }

                                                            if (privateinfos.get("FortuneoLink") == null) {
                                                                Log.i("FortuneoLink"," not defined");
                                                                dictionnaire.put("FortuneoLink", "Non défini par l'utilisateur");

                                                            } else {

                                                                Log.i("Fortu defined",privateinfos.get("FortuneoLink"));
                                                                dictionnaire.put("FortuneoLink", privateinfos.get("FortuneoLink"));

                                                            }

                                                            if (privateinfos.get("CodeGMF") == null) {
                                                                dictionnaire.put("CodeGMF", "Non défini par l'utilisateur");

                                                            } else {
                                                                dictionnaire.put("CodeGMF", privateinfos.get("CodeGMF"));

                                                            }

                                                            if (privateinfos.get("Direct Energie") == null) {
                                                                dictionnaire.put("Direct Energie", "Non défini par l'utilisateur");

                                                            } else {
                                                                dictionnaire.put("Direct Energie", privateinfos.get("Direct Energie"));

                                                            }


                                                            if (privateinfos.get("AXAMail") == null) {
                                                                Log.i("AXA"," not defined");
                                                                dictionnaire.put("AXA", "Non défini par l'utilisateur");

                                                            } else {
                                                                dictionnaire.put("AXA", privateinfos.get("AXAMail"));

                                                            }
                                                            if (privateinfos.get("MAX") == null) {
                                                                dictionnaire.put("MAX", "Non défini par l'utilisateur");
                                                            } else {

                                                                dictionnaire.put("MAX", privateinfos.get("MAX"));

                                                            }

                                                            if (privateinfos.get("Bourse Direct") == null) {
                                                                dictionnaire.put("Bourse Direct", "Non défini par l'utilisateur");
                                                            } else {

                                                                dictionnaire.put("Bourse Direct", privateinfos.get("Bourse Direct"));

                                                            }

                                                            if (privateinfos.get("American Express") == null) {
                                                                dictionnaire.put("American Express", "Non défini par l'utilisateur");
                                                            } else {

                                                                dictionnaire.put("American Express", privateinfos.get("American Express"));

                                                            }


                                                            ParseQuery<ParseUser> querr = ParseUser.getQuery();

                                                            querr.whereEqualTo("username", name_to_catch);

                                                            querr.findInBackground(new FindCallback<ParseUser>() {
                                                                @Override
                                                                public void done(List<ParseUser> objects, ParseException e) {




                                                                    List<Integer> counterlist = ParseUser.getCurrentUser().getList("Score");

                                                                    if (String_caught.equals("Boursorama")) {
                                                                        money = 110;

                                                                    }
                                                                    if (String_caught.equals("Direct Energie")) {
                                                                        money = 30;

                                                                    }

                                                                    if (String_caught.equals("ING")) {
                                                                        money = 80;

                                                                    }

                                                                    if (String_caught.equals("Revolut")) {
                                                                        money = 6;

                                                                    }

                                                                    if (String_caught.equals("AXA")) {
                                                                        money = 75;

                                                                    }

                                                                    if (String_caught.equals("GMF")) {
                                                                        money = 30;

                                                                    }
                                                                    if (String_caught.equals("BNP Paribas") | String_caught.equals("Société Générale") | String_caught.equals("Crédit Agricole")) {
                                                                        money = 30;

                                                                    }

                                                                    if (String_caught.equals("Uber")  ) {
                                                                        money = 10;

                                                                    }
                                                                    if (String_caught.equals("Deliveroo")  ) {
                                                                        money = 5;

                                                                    }
                                                                    if (String_caught.equals("Bolt")  ) {
                                                                        money = 10;

                                                                    }
                                                                    if (String_caught.equals("Saveur Bière")  ) {
                                                                        money = 5;

                                                                    }
                                                                    if (String_caught.equals("MAX")  ) {
                                                                        money = 5;
                                                                    }
                                                                    if (String_caught.equals("Fitness Park La Défense")  ) {
                                                                        money = 30;
                                                                    }

                                                                    if (String_caught.equals("Bourse Direct")  ) {
                                                                        money = 20;

                                                                    }
                                                                    if (String_caught.equals("American Express")  ) {
                                                                        money = 50;

                                                                    }

                                                                    if (counterlist != null) {
                                                                        if (counterlist.size() > 0) {
                                                                            counterlist.add(counterlist.get(counterlist.size() - 1) + (int) Math.round(0.8 * money));

                                                                        }
                                                                    }

                                                                    if (counterlist == null) {

                                                                        ArrayList<Integer> lip = new ArrayList<Integer>();
                                                                        lip.add((int) Math.round(0.8 * money));
                                                                        counterlist = lip;
                                                                    }

                                                                    if (counterlist != null) {
                                                                        if (counterlist.size() == 0) {

                                                                            ArrayList<Integer> lip = new ArrayList<Integer>();
                                                                            lip.add((int) Math.round(0.8 * money));
                                                                            counterlist = lip;
                                                                        }
                                                                    }

                                                                    ParseUser.getCurrentUser().put("Score", counterlist);

                                                                    Date currentTime = Calendar.getInstance().getTime();


                                                                    System.out.print(currentTime);


                                                                    Calendar calendar = Calendar.getInstance();
                                                                    String month = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());


                                                                    List<String> DateList = ParseUser.getCurrentUser().getList("DateTime");
                                                                    if (DateList == null) {
                                                                        ArrayList<String> dater = new ArrayList<String>();
                                                                        dater.add(currentTime.toString());
                                                                        DateList = dater;
                                                                    } else {
                                                                        DateList.add(currentTime.toString());
                                                                    }
                                                                    ParseUser.getCurrentUser().put("DateTime", DateList);

                                                                    ParseUser.getCurrentUser().saveInBackground();

                                                                    //ParseUser.get


                                                                    receiverdico.add(dictionnaire);


                                                                    ParseUser userfound = objects.get(0);


                                                                    Map<String, HashMap<String, List<String>>> receiver_informations = ParseUser.getCurrentUser().getMap("ReceiverInfo");


                                                                    //Log.i("receiverinfo",receiver_informations.toString());
                                                                    if (receiver_informations != null) {

                                                                        Log.i("receiveris ", "not null");
                                                                        if (receiver_informations.containsKey(userfound.getUsername())) {

                                                                            Log.i("user already in dict", userfound.getUsername());
                                                                            for (String key : receiver_informations.keySet()) {

                                                                                Log.i("key", key);
                                                                                Log.i("userfound", userfound.getUsername());
                                                                                Log.i("bool val", String.valueOf(key == userfound.getUsername()));
                                                                                if (key.equals(userfound.getUsername())) {

                                                                                    Log.i("selectiong user", userfound.getUsername());
                                                                                    if (receiver_informations.get(key).containsKey(String_caught)) {
                                                                                        Log.i("bank is in", String_caught);
                                                                                        for (String bank : receiver_informations.get(key).keySet()) {
                                                                                            Log.i("bank", bank);
                                                                                            if (bank.equals(String_caught)) {

                                                                                                String actualdate = Calendar.getInstance().getTime().toString();
                                                                                                receiver_informations.get(userfound.getUsername()).get(bank).add(actualdate);


                                                                                                ParseUser.getCurrentUser().put("ReceiverInfo", receiver_informations);

                                                                                                ParseUser.getCurrentUser().saveInBackground();


                                                                                            }


                                                                                        }
                                                                                    } else {

                                                                                        Log.i("dont contain", "String caught");
                                                                                        List<String> llist = new ArrayList<String>();
                                                                                        String actualdate = Calendar.getInstance().getTime().toString();
                                                                                        llist.add(actualdate);


                                                                                        receiver_informations.get(userfound.getUsername()).put(String_caught, llist);


                                                                                        ParseUser.getCurrentUser().put("ReceiverInfo", receiver_informations);

                                                                                        ParseUser.getCurrentUser().saveInBackground();

                                                                                    }


                                                                                }

                                                                            }
                                                                        } else {

                                                                            List<String> llist = new ArrayList<String>();
                                                                            String actualdate = Calendar.getInstance().getTime().toString();
                                                                            llist.add(actualdate);
                                                                            HashMap<String, List<String>> hm = new HashMap();
                                                                            hm.put(String_caught, llist);


                                                                            receiver_informations.put(userfound.getUsername(), hm);


                                                                            ParseUser.getCurrentUser().put("ReceiverInfo", receiver_informations);

                                                                            ParseUser.getCurrentUser().saveInBackground();


                                                                        }


                                                                    }
                                                                    if (receiver_informations == null) {

                                                                        Log.i("list is null", "creating it");

                                                                        List<String> llist = new ArrayList<String>();
                                                                        String actualdate = Calendar.getInstance().getTime().toString();
                                                                        llist.add(actualdate);
                                                                        HashMap<String, List<String>> hm = new HashMap();
                                                                        hm.put(String_caught, llist);
                                                                        Map m = new ArrayMap();
                                                                        m.put(userfound.getUsername(), hm);

                                                                        Log.i("map", m.toString());


                                                                        ParseUser.getCurrentUser().put("ReceiverInfo", m);

                                                                        ParseUser.getCurrentUser().saveInBackground();


                                                                    }


                                                                    ParseUser.getCurrentUser().saveInBackground();


                                                                    List<Integer> counterlist2 = userfound.getList("Score");

                                                                    if (String_caught.equals("Boursorama")) {
                                                                        money = 110;

                                                                    }

                                                                    if (String_caught.equals("ING") | String_caught.equals("GMF")) {
                                                                        money = 80;

                                                                    }
                                                                    if (String_caught.equals("AXA") ) {
                                                                        money = 75;

                                                                    }
                                                                    if (String_caught.equals("MAX") ) {
                                                                        money = 0;

                                                                    }
                                                                    if (String_caught.equals("Direct Energie") ) {
                                                                        money = 20;

                                                                    }
                                                                    if (String_caught.equals("Fitness Park La Défense")  ) {
                                                                        money = 30;
                                                                    }

                                                                    if (String_caught.equals("Revolut")) {
                                                                        money = 6;

                                                                    }
                                                                    if (String_caught.equals("BNP Paribas") | String_caught.equals("Société Générale") | String_caught.equals("Crédit Agricole")) {
                                                                        money = 30;

                                                                    }

                                                                    if (String_caught.equals("Uber")) {
                                                                        money = 10;

                                                                    }
                                                                    if (String_caught.equals("Deliveroo")) {
                                                                        money = 5;

                                                                    }
                                                                    if (String_caught.equals("Bolt")) {
                                                                        money = 7;

                                                                    }

                                                                    if (String_caught.equals("Saveur Bière")) {
                                                                        money = 5;

                                                                    }
                                                                    if (counterlist2 != null) {


                                                                        try {
                                                                            counterlist2.add(counterlist2.get(counterlist2.size() - 1) + money);
                                                                        } catch (Exception ClassCastException) {
                                                                            ArrayList<Integer> lip2 = new ArrayList<Integer>();
                                                                            lip2.add(money);
                                                                            counterlist2 = lip2;
                                                                        }


                                                                    } else {

                                                                        ArrayList<Integer> lip2 = new ArrayList<Integer>();
                                                                        lip2.add(money);
                                                                        counterlist2 = lip2;
                                                                    }

                                                                    userfound.put("Score", counterlist2);



                                                                    Intent newintent = new Intent(getApplicationContext(), Reciever_Page.class);

                                                                    Log.i("Surname", dictionnaire.get("Surname"));
                                                                    Log.i("DICO",dictionnaire.toString());

                                                                    newintent.putExtra("Surname", dictionnaire.get("Surname"));
                                                                    newintent.putExtra("Name", dictionnaire.get("Name"));
                                                                    newintent.putExtra("Username", dictionnaire.get("Username"));
                                                                    newintent.putExtra("LienParrainage", dictionnaire.get("LienParrainage"));
                                                                    newintent.putExtra("Code", dictionnaire.get("Code"));
                                                                    newintent.putExtra("CodeUber", dictionnaire.get("CodeUber"));
                                                                    newintent.putExtra("Deliveroo", dictionnaire.get("Deliveroo"));
                                                                    newintent.putExtra("MAX", dictionnaire.get("MAX"));
                                                                    newintent.putExtra("Bank", String_caught);
                                                                    newintent.putExtra("email", dictionnaire.get("email"));
                                                                    newintent.putExtra("FortuneoLink", dictionnaire.get("FortuneoLink"));
                                                                    newintent.putExtra("Saveur Bière", dictionnaire.get("SaveurBiere"));
                                                                    newintent.putExtra("CodeGMF", dictionnaire.get("CodeGMF"));
                                                                    newintent.putExtra("Direct Energie", dictionnaire.get("Direct Energie"));
                                                                    newintent.putExtra("AXA", dictionnaire.get("AXA"));
                                                                    newintent.putExtra("Adresse", dictionnaire.get("Adresse"));
                                                                    newintent.putExtra("Ville", dictionnaire.get("Ville"));
                                                                    newintent.putExtra("American Express", dictionnaire.get("American Express"));
                                                                    newintent.putExtra("Bourse Direct", dictionnaire.get("Bourse Direct"));
                                                                    newintent.putExtra("Bolt", dictionnaire.get("Bolt"));
                                                                    //newintent.putExtra("Fitness Park La Défense", dictionnaire.get("Fitness Park La Défense"));
                                                                    newintent.putExtra("FitnessParkSurname", dictionnaire.get("FitnessParkSurname"));
                                                                    newintent.putExtra("FitnessParkName", dictionnaire.get("FitnessParkName"));


                                                                    Integer Counter = ParseUser.getCurrentUser().getInt("Counter");
                                                                    if (Counter != null) {

                                                                        Counter = Counter + 1;
                                                                    } else {

                                                                        Counter = 1;
                                                                    }

                                                                    ParseUser.getCurrentUser().put("Counter", Counter);
                                                                    ParseUser.getCurrentUser().saveInBackground();


                                                                    // Dernier User


/*
                                                                    String p = "Code";

                                                                    if (dictionnaire.get("Code").equals("Non défini par l'utilisateur")) {
                                                                        Log.i("dico get Code", dictionnaire.get("Code"));
                                                                        p = "LienParrainage";
                                                                    }*/

                                                                    String p = "Non défini par l'utilisateur";

                                                                    if (String_caught.equals("Boursorama")) {
                                                                        p=dictionnaire.get("LienParrainage");

                                                                    }

                                                                    if (String_caught.equals("ING") ) {
                                                                       p= dictionnaire.get("Code");

                                                                    }
                                                                    if  (String_caught.equals("GMF")) {
                                                                        p=dictionnaire.get("CodeGMF");

                                                                    }
                                                                    if (String_caught.equals("AXA") ) {
                                                                        p=dictionnaire.get("AXA");

                                                                    }
                                                                    if (String_caught.equals("Direct Energie") ) {
                                                                        p=dictionnaire.get("Direct Energie");

                                                                    }

                                                                    if (String_caught.equals("Revolut")) {
                                                                        Log.i("REVOLUT",p);
                                                                        p=dictionnaire.get("FortuneoLink");

                                                                    }
                                                                    if (String_caught.equals("MAX")) {
                                                                       p=dictionnaire.get("MAX");

                                                                    }


                                                                    if (String_caught.equals("Uber")) {
                                                                       p=dictionnaire.get("CodeUber");

                                                                    }
                                                                    if (String_caught.equals("Deliveroo")) {
                                                                        p=dictionnaire.get("Deliveroo");

                                                                    }
                                                                    if (String_caught.equals("Bolt")) {
                                                                        p=dictionnaire.get("Bolt");

                                                                    }

                                                                    if (String_caught.equals("Saveur Bière")) {
                                                                        p=dictionnaire.get("SaveurBiere");

                                                                    }
                                                                    if (String_caught.equals("American Express")) {
                                                                        p=dictionnaire.get("American Express");

                                                                    }
                                                                    if (String_caught.equals("Bourse Direct")) {
                                                                        p=dictionnaire.get("Bourse Direct");

                                                                    }
                                                                    if (String_caught.equals("Fitness Park La Défense")) {
                                                                        p=dictionnaire.get("Fitness Park La Défense");

                                                                    }



                                                                    Util.Upload_Dictionary("ReceptionMenu", String_caught, userfound, dictionnaire, p);


                                                                    startActivity(newintent);


                                                                }
                                                            });

                                                        } else {
                                                            Log.i("error mess", e.getMessage());
                                                        }
                                                    }
                                                });


                                                if (privateinfos.get("Name") == null) {
                                                    dictionnaire.put("Name", "Non défini par l'utilisateur");
                                                } else {

                                                    dictionnaire.put("Name", privateinfos.get("Name"));

                                                }
                                                if (privateinfos.get("Deliveroo") == null) {
                                                    dictionnaire.put("Deliveroo", "Non défini par l'utilisateur");
                                                } else {

                                                    dictionnaire.put("Deliveroo", privateinfos.get("Deliveroo"));

                                                }
                                                if (privateinfos.get("CodeUber") == null) {
                                                    dictionnaire.put("CodeUber", "Non défini par l'utilisateur");
                                                } else {

                                                    dictionnaire.put("CodeUber", privateinfos.get("CodeUber"));

                                                }
                                                if (privateinfos.get("Bolt") == null) {
                                                    dictionnaire.put("Bolt", "Non défini par l'utilisateur");
                                                } else {

                                                    dictionnaire.put("Bolt", privateinfos.get("Bolt"));

                                                }
                                                if (privateinfos.get("CodeGMF") == null) {
                                                    dictionnaire.put("CodeGMF", "Non défini par l'utilisateur");
                                                } else {

                                                    dictionnaire.put("CodeGMF", privateinfos.get("CodeGMF"));

                                                }
                                                if (privateinfos.get("Direct Energie") == null) {
                                                    dictionnaire.put("Direct Energie", "Non défini par l'utilisateur");
                                                } else {

                                                    dictionnaire.put("Direct Energie", privateinfos.get("Direct Energie"));

                                                }
                                                if (privateinfos.get("LienParrainage") == null) {
                                                    dictionnaire.put("LienParrainage", "Non défini par l'utilisateur");
                                                } else {

                                                    dictionnaire.put("LienParrainage", privateinfos.get("LienParrainage"));

                                                }

                                                if (privateinfos.get("MAX") == null) {
                                                    dictionnaire.put("MAX", "Non défini par l'utilisateur");
                                                } else {

                                                    dictionnaire.put("MAX", privateinfos.get("MAX"));

                                                }

                                                if (privateinfos.get("Mail") == null) {
                                                    dictionnaire.put("Mail", "Non défini par l'utilisateur");
                                                } else {
                                                    dictionnaire.put("Mail", privateinfos.get("Surname"));
                                                }

                                                if (privateinfos.get("Surname") == null) {
                                                    dictionnaire.put("Surname", "Non défini par l'utilisateur");
                                                } else {
                                                    dictionnaire.put("Surname", privateinfos.get("Surname"));
                                                }

                                                if (privateinfos.get("FitnessParkSurname") == null) {
                                                    dictionnaire.put("FitnessParkSurname", "Non défini par l'utilisateur");
                                                } else {
                                                    dictionnaire.put("FitnessParkSurname", privateinfos.get("FitnessParkSurname"));
                                                }
                                                if (privateinfos.get("FitnessParkName") == null) {
                                                    dictionnaire.put("FitnessParkName", "Non défini par l'utilisateur");
                                                } else {
                                                    dictionnaire.put("FitnessParkName", privateinfos.get("FitnessParkName"));
                                                }


                                            }


                                        } else {
                                            Toast.makeText(getApplicationContext(), "Les utilisateurs présents ont atteint leur quota de parrainnage", Toast.LENGTH_LONG).show();
                                        }


                                    } else {

                                        Toast.makeText(BeSponsoredActivity.this, e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                    }


                                }
                            });

                        }
                return false;

                    }



            });



        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        //client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void fillData() {
        Institutions = new ArrayList<>();

        Banks_and_Insurances = new HashMap<>();


        Institutions.add("Banques");
        Institutions.add("Assurances");
        Institutions.add("Services");
        Institutions.add("Loisirs");

        List<String> Banks = new ArrayList<>();

        Banks.addAll(Arrays.asList("ING", "Boursorama", "Revolut","MAX","Bourse Direct","American Express"));
                    /*    "BNP Paribas", "Crédit Agricole","Société Générale"));*/

        List<String> Insurances = new ArrayList<>();

        Insurances.addAll(Arrays.asList("AXA","GMF"));


        List<String> Leisure = new ArrayList<>();

        Leisure.addAll(Arrays.asList("Fitness Park La Défense","Saveur Bière"));

        List<String> Services = new ArrayList<>();

        Services.addAll(Arrays.asList("Uber","Bolt","Direct Energie","Deliveroo"));

        Banks_and_Insurances.put(Institutions.get(0), Banks);

        Banks_and_Insurances.put(Institutions.get(1), Insurances);

        Banks_and_Insurances.put(Institutions.get(2), Services);

        Banks_and_Insurances.put(Institutions.get(3), Leisure);

    }
/*
    *//**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     *//*
    public RecyclerView.SmoothScroller.Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("MainScreen Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new NotificationCompat.Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(NotificationCompat.Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }*/
}
