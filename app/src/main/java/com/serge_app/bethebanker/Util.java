package com.serge_app.bethebanker;

import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by WFTD- on 28/10/2019.
 */

public final class Util {

private Util() {}

public final static int BYTE = 1;

public static void Upload_Validated(String ColumnName , String First_Key, String Second_Key)
{

    Date currentTime = Calendar.getInstance().getTime();
    String mytime = currentTime.toString();

    Map<String, Map<String , List<String> >> dicovalidated = ParseUser.getCurrentUser().getMap(ColumnName);

        if(dicovalidated != null) {
            if (dicovalidated.containsKey(First_Key)) {
                if (dicovalidated.get(First_Key).containsKey(Second_Key)) {
                    dicovalidated.get(First_Key).get(Second_Key).add(mytime);

                    ParseUser.getCurrentUser().put(ColumnName, dicovalidated);
                    ParseUser.getCurrentUser().saveInBackground();
                }
                else{


                    ArrayList<String> mylist = new ArrayList<String>();
                    mylist.add(mytime);



                    dicovalidated.get(First_Key).put(Second_Key,mylist);

                    ParseUser.getCurrentUser().put(ColumnName,dicovalidated);
                    ParseUser.getCurrentUser().saveInBackground();



                }

            } else {

                Map<String , List<String>> newdic = new HashMap<String, List<String>>();
                ArrayList<String> mylist = new ArrayList<String>();
                mylist.add(mytime);

                newdic.put(Second_Key,mylist);

                dicovalidated.put(First_Key, newdic);

                ParseUser.getCurrentUser().put(ColumnName,dicovalidated);
                ParseUser.getCurrentUser().saveInBackground();


            }
        }

            else{

            Map<String, Map<String , List<String> >> otherdico = new HashMap<String, Map<String , List<String> >>();
            Map<String , List<String>> newdic = new HashMap<String, List<String>>();
            ArrayList<String> mylist = new ArrayList<String>();
            mylist.add(mytime);

            newdic.put(Second_Key,mylist);

            otherdico.put(First_Key, newdic);

            ParseUser.getCurrentUser().put(ColumnName,otherdico);
            ParseUser.getCurrentUser().saveInBackground();


        }





}



public static void Upload_Dictionary(String ColumnName , String First_key , ParseUser Second_Key , Map<String, String> dictionnaire  ,
                              String Last_Key) {

    Map<String, List<Map<String, String>> > inforeception = ParseUser.getCurrentUser().getMap(ColumnName);

//
    if (inforeception != null) {

        if (inforeception.containsKey(First_key)) {


                                Map<String,String> dicotemp = new HashMap<String,String>() ;

                                dicotemp.put(Second_Key.getUsername().toString() ,Last_Key);

                                inforeception.get(First_key).add(dicotemp);


                                ParseUser.getCurrentUser().put(ColumnName, inforeception);

                                ParseUser.getCurrentUser().saveInBackground();


                            }





    else {


            List<Map<String, String>> llist = new ArrayList<Map<String, String>>();

                       Map<String,String> dicotemp = new HashMap<String,String>() ;

                        dicotemp.put(Second_Key.getUsername().toString() , Last_Key);

                        llist.add(dicotemp);


                        inforeception.put(First_key, llist);


                        ParseUser.getCurrentUser().put(ColumnName, inforeception);

                        ParseUser.getCurrentUser().saveInBackground();

                    }



        } else {

        Map<String, List<Map<String, String>> > inforeceptionnew = new HashMap<String, List<Map<String, String>> >();

        List<Map<String, String>> llist = new ArrayList<Map<String, String>>();

        Map<String,String> dicotemp = new HashMap<String,String>() ;

        dicotemp.put(Second_Key.getUsername().toString() , Last_Key);

        llist.add(dicotemp);


        inforeceptionnew.put(First_key, llist);


        ParseUser.getCurrentUser().put(ColumnName, inforeceptionnew);

        ParseUser.getCurrentUser().saveInBackground();


        }





}



}
