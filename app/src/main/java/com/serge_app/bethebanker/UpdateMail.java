package com.serge_app.bethebanker;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.parse.FunctionCallback;
import com.parse.ParseACL;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.HashMap;

//import android.support.v7.app.AppCompatActivity;





public class UpdateMail extends AppCompatActivity {

    public void UploadMail(View view)
    {
        EditText edittext = (EditText) findViewById(R.id.MailText);
        final String Mail = edittext.getText().toString();

        HashMap<String,String> params = new HashMap<String, String>();

        params.put("username",ParseUser.getCurrentUser().getUsername());
        params.put("Mail",Mail);



        ParseCloud.callFunctionInBackground("UpdateMail",params,  new FunctionCallback<String>() {
            public void done(String str, ParseException e) {
                if (e == null) {
                    Toast.makeText(getApplicationContext(), "Informations envoyées avec succès", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

        });


    }

    public void RemoveMail(View view)
    {

        HashMap<String,String> params = new HashMap<String, String>();

        params.put("username",ParseUser.getCurrentUser().getUsername());




        ParseCloud.callFunctionInBackground("DestroyMail",params,  new FunctionCallback<String>() {
            public void done(String str, ParseException e) {
                if (e == null) {
                    Toast.makeText(getApplicationContext(), "Mail supprimé avec succès", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

        });


    }

    public void DestroyAllMails(View view)
    {

        HashMap<String,String> params = new HashMap<String, String>();

        params.put("username",ParseUser.getCurrentUser().getUsername());




        ParseCloud.callFunctionInBackground("DestroyCompanyMails",params,  new FunctionCallback<String>() {
            public void done(String str, ParseException e) {
                if (e == null) {
                    Toast.makeText(getApplicationContext(), "Mail supprimé avec succès", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

        });


    }

    public void ChangePassword(View view)
    {


        ParseACL postACL = new ParseACL(ParseUser.getCurrentUser());
        postACL.setPublicReadAccess(true);
        postACL.setPublicWriteAccess(true);
        ParseUser.getCurrentUser().setACL(postACL);
        ParseUser.getCurrentUser().saveInBackground();


        EditText pwedit = (EditText) findViewById(R.id.PWDText) ;
        final String PW = pwedit.getText().toString();
        Log.i("EDTEXT",pwedit.getText().toString());

        EditText pwconfirm = (EditText) findViewById(R.id.PWDConfirm) ;
        String PWDConfirm = pwconfirm.getText().toString();
        Log.i("PW",PW);
        Log.i("PWD",PWDConfirm);
        if(PW.equals(PWDConfirm))
        {
            ParseUser currentUser = ParseUser.getCurrentUser();

            Log.i("PW",PW);
            currentUser.setPassword(PW);
            currentUser.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if(e == null)
                    {

                        Toast.makeText(getApplicationContext(),"Changement de mot de Passe effectué",Toast.LENGTH_LONG).show();

                        SharedPreferences prefs = getSharedPreferences("UserData", MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();

                            editor.putString("Password", PW);
                            editor.commit();

                    }
                    else
                    {

                        Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
            });


            postACL.setPublicReadAccess(true);
            postACL.setPublicWriteAccess(false);
            ParseUser.getCurrentUser().setACL(postACL);
            ParseUser.getCurrentUser().saveInBackground();
        }

        else{

            Toast.makeText(getApplicationContext(),"Les mots de passe ne sont pas identiques",Toast.LENGTH_LONG).show();;
        }




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

       /* ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);*/

        MenuInflater menuInflater = new MenuInflater(this);

        menuInflater.inflate(R.menu.bethebankermenu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {



        if(item.getItemId()==R.id.MainPage)
        {

            Intent graphintent = new Intent(this,ActivityChoice.class);
            startActivity(graphintent);

        }

        if(item.getItemId()==R.id.Disclaimer)
        {
            Intent graphintent = new Intent(this,PolitiqueConfidentalite.class);
            startActivity(graphintent);

        }

        if(item.getItemId()==R.id.Classement)
        {
            Intent scrollList = new Intent(this,ScrollListActivity.class);
            startActivity(scrollList);


        }

        if(item.getItemId()==R.id.GraphPage)
        {

            Intent graphintent = new Intent(this,GraphActivity.class);
            startActivity(graphintent);

        }

        if(item.getItemId() == R.id.Logout) {

            ParseUser.logOut();

            Intent intent = new Intent(getApplicationContext(),MainActivity.class);

            startActivity(intent);

        }

        if(item.getItemId() == R.id.Destroy) {

            Intent intent = new Intent(getApplicationContext(),DestroyUser.class);

            startActivity(intent);

        }

        if(item.getItemId() == R.id.RecapReceive) {

            Intent intent = new Intent(getApplicationContext(),RecapMenu.class);

            startActivity(intent);

        }

       if(item.getItemId() == R.id.UploadMail) {

           Intent intent = new Intent(getApplicationContext(), UpdateMail.class);

           startActivity(intent);
       }


        return super.onOptionsItemSelected(item);
    }



    public void comp_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), ScrollListActivity.class);
        startActivity(graphintent);

    }

    public void graph_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), GraphActivity.class);
        startActivity(graphintent);

    }

    public void settings_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), UpdateMail.class);
        startActivity(graphintent);

    }

    public void mail_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), RecapMenu.class);
        startActivity(graphintent);

    }

    public void backarrow(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), ActivityChoice.class);
        startActivity(graphintent);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_mail);



        TextView tv = findViewById(R.id.DescriptionMail);

       /* String str =
                "<font color=#1F2359>Renseignez votre adresse si vous " +"<br>"+
                        "souhaitez être alerté lorsque <br />" +"<br>"+
                        "votre code de Parrainnage <br />" +"<br>"+
                        "est utilisé <br /> </font>" ;


        String str2 =
                "Renseignez votre adresse si vous souhaitez être alerté lorsque votre code de Parrainnage est utilisé" ;


                       // tv.setText(Html.fromHtml(str));
        tv.setText(str2);
*/        //ImageView imgview = findViewById(R.id.ImageSkyscrapers);

       // imgview.setScaleType(ImageView.ScaleType.FIT_XY);

    }
}
